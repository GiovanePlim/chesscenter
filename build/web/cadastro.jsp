<%-- 
    Document   : cadastro
    Created on : 13/06/2015, 03:44:35
    Author     : GIOVANE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro - xadrezweb</title>


        <script  src="js/jquery-1.11.2.js" type="text/javascript" ></script>
        <link rel="stylesheet" href="css/bootstrap/bootstrap-3.3.4.css">
        <script src="js/bootstrap/bootstrap-3.3.4.js"></script>
        <script src="js/funcoes.js"></script>
        <link rel="stylesheet" href="css/funcoes.css">
        <script src="js/Cadastro.js"></script>







    </head>
    <body>


        <div class="col-md-12" style="padding: 10%; ">

            <div  id="cad-usuario" class="col-md-4" style="background: steelblue; border: slategray 1px solid; border-radius: 2px;">



                <form class="form-horizontal"  id="form_cadastro" action="usuario" method="post">
                    <fieldset>

                        <!-- Form Name -->
                        <legend style="color: white">Cadastro de jogador</legend>

                        <!-- Text input-->
                        <div class="form-group"> 
                            <div class="col-md-12">
                                <input id="txt_nome" name="txt_nome" placeholder="Nome completo" class="form-control input-lg" required="" type="text">

                            </div>
                        </div>

                        <!-- Text input-->
                        <span id="aviso_email_existe" style="display: none;  margin-left:  12px; color: white; font-size: 16px;">Atenção, esse email já está cadastrado.</span>
                        <span id="aviso_email_invalido" style="display: none;  margin-left:  12px; color: white; font-size: 16px;">Email inválido.</span>
                        <div class="form-group"> 
                            <div class="col-md-12">
                                <input id="txt_email" name="txt_email" placeholder="Email" class="form-control input-lg" required="" type="text">

                            </div>
                        </div>
                        
                        <div id="load"></div>

                        <!-- Text input-->
                        
                        <span id="aviso_login_existe" style="display: none;  margin-left:  12px; color: white; font-size: 16px;">Atenção, esse login já existe.</span>
                        <div class="form-group">  
                            <div class="col-md-12">
                                <input id="txt_login" name="txt_login" placeholder="Login" class="form-control input-lg" required="" type="text">

                            </div>
                        </div>

                        <!-- Password input-->
                        <div class="form-group">
                            <div class="col-md-12">
                                <input id="txt_senha" name="txt_senha" placeholder="Senha" class="form-control input-lg" required="" type="password">

                            </div>
                        </div>

                        <!-- Password input-->

                        <div class="form-group">
                            <span id="aviso_senhas_diferentes" style="display: none;  margin-left:  12px; color: white; font-size: 16px;">Atenção, senhas divergem.</span>
                            <span id="aviso_senhas_iguais" style="display: none;  margin-left:  12px; color: greenyellow; font-size: 16px;">Senhas iguais.</span>
                            <div class="col-md-12">
                                <input id="txt_repete_senha" name="txt_repete_senha" placeholder="Repita a senha" class="form-control input-lg" required=""  onkeyup="valida()" type="password">

                            </div>
                        </div>

                        <!-- Button (Double) -->
                        <div class="form-group">
                            <div class="col-lg-12">
                                <button id="bt_salvar" name="bt_salvar" class="btn-lg btn-success">Cadastrar</button>
                            </div>
                        </div>

                    </fieldset>
                </form>


            </div>


            <div  class="col-md-8">

                <div class="col-md-2"></div>
                <div class="col-md-10">
                    <h3>Cadastre-se gratuitamente, e divirta-se com pessoas do mundo inteiro!</h3>
                    <img src="img/xadrez_circulo.jpg" width="60%" height="60%" alt="xadrez">
                </div>

            </div>

        </div>


        

    </body>
</html>


