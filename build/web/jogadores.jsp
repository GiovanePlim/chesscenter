<%-- 
    Document   : jogadores
    Created on : 13/06/2015, 23:35:20
    Author     : GIOVANE
--%>

<%@page import="br.com.chesscenter.pojo.JogadorPojo"%>
<%@page import="java.util.List"%>
<%@page import="br.com.chesscenter.dao.JogadorDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div  id="lista_jogadores" class="list-group">

    <%

        JogadorDAO jogadorDAO = new JogadorDAO();

        List<JogadorPojo> jogadores = jogadorDAO.buscarJogadores();

        if (jogadores != null) {

            for (int i = 0; i < jogadores.size(); i++) {

                JogadorPojo jogador = jogadores.get(i);

                if (!jogador.getId().equals(String.valueOf(request.getSession().getAttribute("id_usuario_sessao")))) {


    %>
    <div>

        <a class="list-group-item" style="text-align: left; cursor: pointer;  /*transition:all 100ms linear; z-index:999; */"   id="jogador_<%=jogador.getId()%>" onclick="menuUsuario('<%=jogador.getId()%>')" >
            <%= jogador.getNome()%><span id='status_<%=jogador.getId()%>' class="pull-right" style="text-align: right; color: red">OFF</span>
            <input type="hidden" id="conf_<%=jogador.getId()%>" value="0" >
        </a>
        

    </div>

    <%
                }

            }
        }

    %>


</div>


