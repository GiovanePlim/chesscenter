/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var wsUri = "ws://" + document.location.host + "/ChessCenter/CCWS?id_usuario="+id_usuario;
var websocket = new WebSocket(wsUri);


websocket.onerror = function(evt) { 
    onError(evt); 
};

function enviaMovimento(id_destinatario, id_tabuleiro, casaInicial, casaFinal, cor, id_partida, conteudo, tipo_peca){
    
    var mensagem = {
        
        tipo: "movimento",
        id_remetente: id_usuario,
        id_destinatario: id_destinatario,
        id_partida: id_partida,
        conteudo: conteudo,
        cor: cor,
        casaInicial: casaInicial,
        casaFinal: casaFinal,
        id_tabuleiro: id_tabuleiro,
        id_usuario_mecheu: id_usuario,
        tipo_peca: tipo_peca
        
    }
    
    
    mensagem = JSON.stringify(mensagem);
    
    websocket.send(mensagem);
}



function onError(evt) {
    //writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
}

//AO SE CONECATAR COM O SERVIDOR WEBSOCKET
websocket.onopen = function(evt) {
    onOpen(evt); 
};

function onOpen(evt) {
    
}


//AO RECEBER UMA NOVA MENSAGEM
websocket.onmessage = function(evt) {
    trataMensagem(evt); 
};


function trataMensagem(msg){
    
    msg = msg.data;
    msg = JSON.parse(msg);
    
    
    if ( msg.tipo == "aviso" ){
        if ( msg.conteudo == "esta_online" )
            estaOnLine(msg.id_remetente);
         else
          if ( msg.conteudo == "esta_offline" )
              estaOffLine(msg.id_remetente);
    }else 
        if ( msg.tipo == "convite" ){
            if ( msg.conteudo == "confronto" )
                menuConvite(msg.id_remetente);
            else
                if ( msg.conteudo == "novo_confronto" )
                    criaTabuleiroConfronto(msg.id_partida, msg.id_remetente, msg.cor);
        }
    else 
        if ( msg.tipo == "movimento" ){
            movePeca(msg.id_remetente, msg.id_tabuleiro, msg.casaInicial, msg.casaFinal, msg.cor, msg.id_usuario_mecheu, msg.conteudo, msg.tipo_peca);
        }
}



