<%-- 
    Document   : index
    Created on : 13/06/2015, 03:43:22
    Author     : GIOVANE
--%>

<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <title>Tabuleiro</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

      <link rel="stylesheet" href="css/jqui.css">
        <script src="js/jq.js"></script>
        <script src="js/jqui.js"></script>
        <link rel="stylesheet" href="/resources/demos/style.css">
        
        <script>
            var jq = $.noConflict();
            
            function tabs(){
                jq(function() {
                    jq( "#tabuleiros" ).tabs();
                });
            }

               //jq( "#tabuleiros li" ).delegate("click", "change", "mouseouver"); 
            
              
              

    function carregaDrop(){
    
    /*
        jq(".peca").draggable({
                        appendTo: "div",
                        cursor: "move",
                        //helper: "original",
                        revert: "invalid",
                        revertDuration: 100,
                        drag: function(event, ui){
                            //jq(this).parent().unbind("click");
                            seleciona(jq(this).parent()[0]);
                            carregaDrop();
                            //selecionado = "";
                        }
                    });




                    jq(".marca_movimento").droppable({
                    tolerance: "intersect",
                    accept: ".peca",
                    activeClass: "ui-state-default",
                    hoverClass: "ui-state-hover",
                    drop: function (event, ui)
                    {
                        
                        movimenta(jq(ui.draggable).parent().parent().attr("id"), jq(ui.draggable).parent()[0], jq(this)[0]);
                        event.preventDefault();
                        
                        }
                    });
              
        */
        
        }
              
              
              
              
              

              
        </script>
        
        
        <link rel="stylesheet"  href="css/bootstrap/bootstrap-3.3.4.css">
        <link rel="stylesheet"  href="css/cc.css">
        <script src="js/bootstrap/bootstrap-3.3.4.js" ></script>
        <script src="js/Partida.js" ></script>
        
        <script>
            
            var id_sessao = "<%=request.getSession().getId()%>";
            var id_usuario = "<%=request.getSession().getAttribute("id_usuario_sessao")%>";
            var pisca_usuario = [];
             
            
        </script>
        
        <script src="js/Usuario.js" ></script>
        <script src="js/CCWS.js" ></script>

    </head>
    <body>

        <div class="container">
            <header class="row" style="height: 100px;" id="header">
                <div class="btn-group pull-right" role="group" aria-label="..." style="top: 90%;">
                    <button type="button" class="btn btn-default glyphicon glyphicon-log-out" onclick="logof()"> logof</button>
                </div>
                <h1 id="altura"> Bem vindo <%= request.getSession().getAttribute("nome_usuario_sessao")%> </h1>
            </header>
            
            <jsp:include page="modais.jsp" />
            
            <div class="row">
                <!-- <div id="tabuleiros" role="main" class="col-md-9 col-md-push-3" style="padding: 10px;"> -->

                    <div id="tabuleiros" role="main" class="col-md-9 col-md-push-3" style="padding: 10px;">
                        
                        
                    
                    </div> 

               <!-- </div> -->
                
                <nav class="col-md-3 col-md-pull-9">
                    <jsp:include page="jogadores.jsp" /> 
                </nav>
            </div>
            <footer class="row">

            </footer>
        </div>    


    </body>
</html>
