/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.chesscenter.websocket;

import br.com.chesscenter.pojo.MensagemPojo;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.websocket.EncodeException;
import javax.websocket.OnMessage;
import javax.websocket.server.ServerEndpoint;
import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;

/**
 *
 * @author GIOVANE
 */
@ServerEndpoint("/CCWS")
public class ChessCenterWS {

    
    private static Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());
    
    
    @OnMessage
    public void onMessage(String mensagemPojoJson) throws IOException, EncodeException {
        Gson gson = new Gson();
        
        MensagemPojo mensagemPojo = gson.fromJson(mensagemPojoJson, MensagemPojo.class);
        
        this.trataMensagem(mensagemPojo);
        
    }
    
    public void trataMensagem(MensagemPojo mensagemPojo) throws IOException, EncodeException{
        
        if ( mensagemPojo.getTipo().equals("movimento") ){
        
            //POIS SERAO DUAS MENSAGENS
            MensagemPojo mensagemPojo2 =  new MensagemPojo();
            
            mensagemPojo2.setConteudo(mensagemPojo.getConteudo());
            mensagemPojo2.setTipo(mensagemPojo.getTipo());
            mensagemPojo2.setId_destinatario(mensagemPojo.getId_remetente());
            mensagemPojo2.setId_usuario_mecheu(mensagemPojo.getId_destinatario());
            mensagemPojo2.setId_remetente(mensagemPojo.getId_destinatario());
            mensagemPojo2.setCasaInicial(mensagemPojo.getCasaInicial());
            mensagemPojo2.setCasaFinal(mensagemPojo.getCasaFinal());
            mensagemPojo2.setId_tabuleiro(mensagemPojo.getId_tabuleiro());
            mensagemPojo2.setId_partida(mensagemPojo.getId_partida());
            mensagemPojo2.setCor(mensagemPojo.getCor());
            mensagemPojo2.setTipo_peca(mensagemPojo.getTipo_peca());
            
            
            this.mensagemPara(mensagemPojo);
            this.mensagemPara(mensagemPojo2);
            
        }
        
        
    }
    
    
    //CHAMADO AO ABRIR CONEXAO COM O SERVIDOR
    @OnOpen
    public void onOpen (Session peer) throws IOException, Exception {
        
        //ADCIONA NA LISTA DE USUÁRIOS CONECTADOS NO SERVIDOR
        peers.add(peer);
        
        
        MensagemPojo mensagemPojo = new MensagemPojo();
        mensagemPojo.setId_remetente(peer.getPathParameters().get("id_usuario"));
        mensagemPojo.setTipo("aviso");
        mensagemPojo.setConteudo("esta_online");
        
        this.mensagemDe(mensagemPojo);
        
        
        
    }
   
    public void mensagemPara(MensagemPojo mensagemPojo) throws IOException, EncodeException{
        
         Gson gson = new Gson();
        String mensagemJson = gson.toJson(mensagemPojo);
        
        for ( Session peer : peers){
            if ( peer.getPathParameters().get("id_usuario").equals(mensagemPojo.getId_destinatario()) ){
                peer.getBasicRemote().sendText(mensagemJson);
            }
                
        }
        
    }
    
    public void mensagemDe(MensagemPojo mensagemPojo) throws IOException{
        
        Gson gson = new Gson();
        String mensagemJson = gson.toJson(mensagemPojo);
        
        for ( Session peer : peers){
            if ( !peer.getPathParameters().get("id_usuario").equals(mensagemPojo.getId_remetente()) )
                peer.getBasicRemote().sendText(mensagemJson);
        }
        
    }
    

    @OnClose
    public void onClose (Session peer) throws IOException {
        peers.remove(peer);
        
        MensagemPojo mensagemPojo = new MensagemPojo();
        mensagemPojo.setId_remetente(peer.getPathParameters().get("id_usuario"));
        mensagemPojo.setTipo("aviso");
        mensagemPojo.setConteudo("esta_offline");
        
        this.mensagemDe(mensagemPojo);
        
    }
    
    public void atualiza_lista_status(String id_usuario) throws IOException, EncodeException{
        
        MensagemPojo mensagemPojo = new MensagemPojo();
        
        mensagemPojo.setId_destinatario(id_usuario);
        mensagemPojo.setTipo("aviso");
        mensagemPojo.setConteudo("esta_online");
        
        for ( Session peer : peers){
            
                if ( !peer.getPathParameters().get("id_usuario").equals(id_usuario) )
                    mensagemPojo.setId_remetente(peer.getPathParameters().get("id_usuario"));
                    this.mensagemPara(mensagemPojo);
            }
                
        }
        
    
    
}
