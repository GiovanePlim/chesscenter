/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.chesscenter.bean;

/**
 *
 * @author GIOVANE
 */
public class Torre extends Peca{

    
    public boolean movimenta(int casaInicial, int casaDestino, String cor, String[] tabuleiro) {
        
        boolean mesmaLinha = this.mesmaLinha(casaInicial, casaDestino);
        boolean mesmaColuna = this.mesmaColuna(casaInicial, casaDestino);
        
        if ( mesmaLinha )
            System.out.print("Mesma Linha");
        
        
        if ( !mesmaLinha && !mesmaColuna )
            return false;
        
        
        int casaMaior = 0;
        int casaMenor = 0;
        
        
         //ORDENA AS CASAS
        if ( casaDestino > casaInicial ){
            casaMaior = casaDestino;
            casaMenor = casaInicial;
        }
        else{
            casaMaior = casaInicial;
            casaMenor = casaDestino;
        }
        
        if ( mesmaLinha ){
            for ( int i = (casaMenor+1); i < casaMaior ; i++  )
            {
                if ( !tabuleiro[i].equals("") )
                    return false;
            }    
        }else{
            for ( int i = (casaMenor+1); i < casaMaior ; i++  )
            {
                if ( (casaMaior - i) % 8 == 0 )
                    if ( !tabuleiro[i].equals("") )
                        return false;
            }
        }
        
        return true;
    }
    
}
