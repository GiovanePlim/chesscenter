/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.chesscenter.bean;

/**
 *
 * @author GIOVANE
 */
public class Peao extends Peca{

    private boolean movimentado = false;
   
    public boolean movimenta(int casaInicial, int casaDestino, String cor, String[] tabuleiro) {
        
        
        int casaMaior;
        int casaMenor;
        
        System.out.print("\n\n\nCasa inicial: "+casaInicial);
        System.out.print("\n\n\nCasa final: "+casaDestino);
        System.out.print("\n\n\nCasa Cor: "+cor);
        
        //SE FOR UM  MOVIMENTO PARA TRAS
        if ( cor.equals("branco") ){
            if ( casaDestino >  casaInicial )
                return false;
        }else{
            if( casaDestino < casaInicial )
                return false;
        }
        
        //ORDENA AS CASAS
        if ( casaDestino > casaInicial ){
            casaMaior = casaDestino;
            casaMenor = casaInicial;
        }
        else{
            casaMaior = casaInicial;
            casaMenor = casaDestino;
        }
        //DISTÂNCIA DAS CASAS
        int calculo = casaMaior - casaMenor;
        
        boolean movimentoCaptura = false;
        boolean movimentoPraFrente = false;
        
        //SE FOI UM MOVIMENTO EM DIAGONAL
        if ( calculo == 7 || calculo == 9 || calculo == 13){
            movimentoCaptura = true;
            movimentoPraFrente = false;
            
        }else 
            //MOVEU PARA FRENTE
            if ( calculo == 16 || calculo == 8 ){
                movimentoCaptura = false;
                movimentoPraFrente = true;
            }
            
        // SE FOR UM MOVIMENTO DE CAPTURA
        if ( movimentoCaptura )
        {
            //SE A COR FOR DIFERENTE DA PECA DA CASA DE DESTINO E SE A CASA DE DESTINO NÃO ESTIVER VAZIA
            if ( !cor.equals(tabuleiro[casaDestino]) && !tabuleiro[casaDestino].equals(""))
                return true;
            else
                return false;
        
        }else 
            if ( movimentoPraFrente ){
                
                if ( ( cor.equals("branco") && !this.primeiraColunaDasBrancas(casaInicial) ) || ( cor.equals("preto") && !this.primeiraColunaDasNegras(casaInicial))){
                    //SE MOVEU MAIS DE UMA CASA PARA FRENTE
                    if ( calculo >8 )
                        return false;
                    // SE HOUVER PECA
                    if ( !tabuleiro[casaDestino].equals("") )
                        return false;
                    
                    return true;
                }else{
                    if ( calculo == 16 ){
                        
                        if ( cor.equals("branco") ){
                            if ( !this.primeiraColunaDasBrancas(casaInicial) )
                                return false;
                        }else{
                            if ( !this.primeiraColunaDasNegras(casaInicial) )
                                return false;
                        }
                        
                        //SE MOVIMENTOU DUAS CASAS E A DO MEIO ESTA OCUPADA
                         if ( !tabuleiro[(casaMaior - 8)].equals("") )
                            return false;
                         //SE A CASA DE DESTINO ESTA OCUPADA
                        if ( !tabuleiro[casaDestino].equals("") )
                            return false;
                    }else{
                        //SE A CASA DE DESTINO ESTA OCUPADA
                        if ( !tabuleiro[casaDestino].equals("") )
                            return false;
                    }
                    
                    
                }
                return true;
            }else
            //SE FOR OUTRO RESULTADO É UMA MOVIMENTO INVÁLIDO PARA O PEAO
            return false;
        
    }

    public boolean primeiraColunaDasBrancas(int casa){
    
        if ( casa >= 48 && casa <= 55 )
            return true;
        
        return false;
    }
    
    public boolean primeiraColunaDasNegras(int casa){
    
        if ( casa >= 8 && casa <= 15 )
            return true;
        
        return false;
    }
    
}
