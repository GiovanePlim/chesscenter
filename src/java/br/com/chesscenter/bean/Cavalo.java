/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.chesscenter.bean;

/**
 *
 * @author GIOVANE
 */
public class Cavalo extends Peca{

    
    public boolean movimenta(int casaInicial, int casaDestino, String cor, String[] tabuleiro) {
        
        int casaMaior = 0;
        int casaMenor = 0;
        
         //ORDENA AS CASAS
        if ( casaDestino > casaInicial ){
            casaMaior = casaDestino;
            casaMenor = casaInicial;
        }
        else{
            casaMaior = casaInicial;
            casaMenor = casaDestino;
        }
        //DISTÂNCIA DAS CASAS
        int calculo = casaMaior - casaMenor;
        
        boolean mudouDeLinha = this.outraLinha(casaInicial, casaDestino);
        
        
        if ( calculo == 6 || calculo == 10 ||  calculo ==  15 || calculo == 17 )
            if ( mudouDeLinha )
                return true;
        
        
        return false;
        
        
    }
    
}
