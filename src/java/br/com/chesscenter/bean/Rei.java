/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.chesscenter.bean;

/**
 *
 * @author GIOVANE
 */
public class Rei extends Peca{

    public boolean movimenta(int casaInicial, int casaDestino, String cor, String[] tabuleiro) {
        int casaMaior = 0;
        int casaMenor = 0;
        
        
         System.out.print("\n\n\nCasa inicial: "+casaInicial);
        System.out.print("\n\n\nCasa final: "+casaDestino);
        
        
         //ORDENA AS CASAS
        if ( casaDestino > casaInicial ){
            casaMaior = casaDestino;
            casaMenor = casaInicial;
        }
        else{
            casaMaior = casaInicial;
            casaMenor = casaDestino;
        }
        
        int calculo = casaMaior - casaMenor;
        
        if ( calculo == 8 || calculo == 9 || calculo == 1 || calculo == 7 )
            return true;
        
        return false;
        
    }
    
}
