/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.chesscenter.bean;

/**
 *
 * @author GIOVANE
 */
abstract class Peca{
    
    private String cor;
    
    abstract boolean movimenta(int casaInicial, int casaDestino, String cor, String[] tabuleiro);

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }
    
    public boolean outraLinha(int casaInicial, int casaDestino){
       
        return !this.mesmaLinha(casaInicial, casaDestino);
    
    }
    
    public boolean mesmaColuna(int casaInicial, int casaDestino){
        
        int casaMaior = 0;
        int casaMenor = 0;
        
        
         //ORDENA AS CASAS
        if ( casaDestino > casaInicial ){
            casaMaior = casaDestino;
            casaMenor = casaInicial;
        }
        else{
            casaMaior = casaInicial;
            casaMenor = casaDestino;
        }
        
        int calculo = casaMaior - casaMenor;
        
        if ( ( (calculo % 8 ) == 0 ) && this.outraLinha(casaInicial, casaDestino))
            return true;
        
        return false;

    }
    
    public boolean mesmaLinha(int casaInicial, int casaDestino){
        
        if ( ( casaInicial >= 0 && casaInicial <= 7 ) && ( casaDestino >=0 && casaDestino <=7 ) ){
            return true;
        }
        
        else
            
        if ( ( casaInicial >= 8 && casaInicial <= 15 ) && ( casaDestino >=8 && casaDestino <=15 ) ){
            return true;
        }
        else
        
        if ( ( casaInicial >= 16 && casaInicial <= 23 ) && ( casaDestino >=16 && casaDestino <=23 ) ){
            return true;
        }
        else
            
        if ( ( casaInicial >= 24 && casaInicial <= 31 ) && ( casaDestino >=24 && casaDestino <=31 ) ){
            return true;
        }
        
        else
        if ( ( casaInicial >= 32 && casaInicial <= 39 ) && ( casaDestino >=32 && casaDestino <=39 ) ){
            return true;
        }
        else
            
        if ( ( casaInicial >= 40 && casaInicial <= 47 ) && ( casaDestino >=40 && casaDestino <=47 ) ){
            return true;
        }
        else
            
        if ( ( casaInicial >= 48 && casaInicial <= 55 ) && ( casaDestino >=48 && casaDestino <=55 ) ){
            return true;
        }
        else
            
        if ( ( casaInicial >= 56 && casaInicial <= 63 ) && ( casaDestino >=56 && casaDestino <=63 ) ){
            return true;
        }
        
        return false;
        
    }
    
    
    public int mesmaDiagonal(int casaInicial, int casaDestino){
        
        int casaMaior = 0;
        int casaMenor = 0;
        
        
         //ORDENA AS CASAS
        if ( casaDestino > casaInicial ){
            casaMaior = casaDestino;
            casaMenor = casaInicial;
        }
        else{
            casaMaior = casaInicial;
            casaMenor = casaDestino;
        }
        
        int calculo = casaMaior - casaMenor;
        
        if ( ( calculo % 7 == 0 ) &&  this.outraLinha(casaInicial, casaDestino) ){
        
            return 7;
        }else if ( ( calculo % 9 == 0 ) && this.outraLinha(casaInicial, casaDestino) ){
            
            return 9;
        }
        
        return 0;
                
    }
    
    
}
