/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.chesscenter.bean;

/**
 *
 * @author GIOVANE
 */
public class Bispo extends Peca{

    
    public boolean movimenta(int casaInicial, int casaDestino, String cor, String[] tabuleiro) {
        
        int mesmaDiagonal = this.mesmaDiagonal(casaInicial, casaDestino);
        
        
        if ( mesmaDiagonal == 0 )
            return false;
        
        int casaMaior = 0;
        int casaMenor = 0;
        
        
         System.out.print("\n\n\nCasa inicial: "+casaInicial);
        System.out.print("\n\n\nCasa final: "+casaDestino);
        
        
         //ORDENA AS CASAS
        if ( casaDestino > casaInicial ){
            casaMaior = casaDestino;
            casaMenor = casaInicial;
        }
        else{
            casaMaior = casaInicial;
            casaMenor = casaDestino;
        }
        
        for ( int i = (casaMenor+1); i < casaMaior ; i++  )
        {
            if(  (  (casaMaior - i ) % mesmaDiagonal == 0   ) &&  !tabuleiro[i].equals("") )
                return false;
        }
        
        return true;
    }
    
}
