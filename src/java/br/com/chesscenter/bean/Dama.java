/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.chesscenter.bean;

/**
 *
 * @author GIOVANE
 */
public class Dama extends Peca{

    public boolean movimenta(int casaInicial, int casaDestino, String cor, String[] tabuleiro) {

        Bispo bispo = new Bispo();
        
        if ( bispo.movimenta(casaInicial, casaDestino, cor, tabuleiro) ){
            return true;
        }else
        {
            Torre torre = new Torre();
            
            if ( torre.movimenta(casaInicial, casaDestino, cor, tabuleiro) )
                return true;
        }
        
        return false;
    }
    
}
