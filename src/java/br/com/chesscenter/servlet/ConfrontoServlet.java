/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.chesscenter.servlet;

import br.com.chesscenter.bean.Bispo;
import br.com.chesscenter.bean.Cavalo;
import br.com.chesscenter.bean.Dama;
import br.com.chesscenter.bean.Peao;
import br.com.chesscenter.bean.Rei;
import br.com.chesscenter.bean.Torre;
import br.com.chesscenter.pojo.MensagemPojo;
import br.com.chesscenter.websocket.ChessCenterWS;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



/**
 *
 * @author GIOVANE
 */
@WebServlet(name = "ConfrontoServlet", urlPatterns = {"/ConfrontoServlet"})
public class ConfrontoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("application/json");
        PrintWriter out = response.getWriter(); 
        String resposta = "";
        
        ServletContext  aplicacao = request.getServletContext();
        HttpSession sessao = request.getSession();
        
        try {
            
            //adversario: idAdversario, acao: "movimenta", cor: cor, tipoPeca: tipo, casaInicial: casaInicial, casaDestino: casaDestino, tabuleiro: tabululeiro,  id_tabuleiro: id_tabuleiro
                                    
            String id_tabuleiro = request.getParameter("id_tabuleiro");
            String id_adversario = request.getParameter("adversario");
            String acao = request.getParameter("acao");
            String tipoPeca = request.getParameter("tipoPeca");
            int casaInicial = Integer.parseInt(request.getParameter("casaInicial")) ;
            int casaDestino = Integer.parseInt(request.getParameter("casaDestino")) ;
            String cor = request.getParameter("cor");
            String tabuleiro[] = request.getParameter("tabuleiro").split(",");
            
             if ( acao.equals("movimenta") ){
                if(tipoPeca.equals("peao"))
                {                
                  Peao peao = new Peao();

                  if ( peao.movimenta(casaInicial, casaDestino, cor, tabuleiro) ){
                      resposta = "ok";
                  }else{
                      resposta = "not ok";
                  }

                }
                else
                    if(tipoPeca.equals("torre"))
                    {
                        Torre torre = new Torre();

                        if ( torre.movimenta(casaInicial, casaDestino, null, tabuleiro) ){
                            resposta = "ok";
                        }else{
                            resposta = "not ok";
                        }   
                    }
                else
                    if(tipoPeca.equals("rei"))
                    {
                        Rei rei = new Rei();

                        if ( rei.movimenta(casaInicial, casaDestino, null, null) ){
                            resposta = "ok";
                        }else{
                            resposta = "not ok";
                        }  
                    }
                else
                    if(tipoPeca.equals("cavalo"))
                    {
                        Cavalo cavalo = new Cavalo();

                        if ( cavalo.movimenta(casaInicial, casaDestino, null, null) ){
                            resposta = "ok";
                        }else{
                            resposta = "not ok";
                        }  
                    }
                else
                    if(tipoPeca.equals("bispo"))
                    {
                      Bispo bispo = new Bispo();

                        if ( bispo.movimenta(casaInicial, casaDestino, null, tabuleiro) ){
                            resposta = "ok";
                        }else{
                            resposta = "not ok";
                        }  
                    }
                else
                    if(tipoPeca.equals("dama"))
                    {
                        Dama dama = new Dama();

                        if ( dama.movimenta(casaInicial, casaDestino, null, tabuleiro) ){
                            resposta = "ok";
                        }else{
                            resposta = "not ok";
                        } 
                    }
                
                if ( resposta.equals("ok") ){
                    
                    MensagemPojo mensagemPojo1 = new MensagemPojo();
                    MensagemPojo mensagemPojo2 = new MensagemPojo();
                    ChessCenterWS chessCenterWS = new ChessCenterWS();
                    
                    mensagemPojo1.setId_destinatario(id_adversario);
                    mensagemPojo1.setId_remetente((String)request.getSession().getAttribute("id_usuario_sessao"));
                    mensagemPojo1.setCor(cor);
                    mensagemPojo1.setCasaInicial(String.valueOf(casaInicial) );
                    mensagemPojo1.setCasaFinal(String.valueOf(casaDestino));
                    mensagemPojo1.setId_tabuleiro(id_tabuleiro);
                    mensagemPojo1.setTipo("movimento");
                    mensagemPojo1.setConteudo("movimento");
                    mensagemPojo1.setId_usuario_mecheu((String)request.getSession().getAttribute("id_usuario_sessao"));
                    
                    
                    
                    mensagemPojo2.setId_destinatario((String)request.getSession().getAttribute("id_usuario_sessao"));
                    mensagemPojo2.setId_remetente(id_adversario);
                    mensagemPojo2.setCor(cor);
                    mensagemPojo2.setCasaInicial(String.valueOf(casaInicial) );
                    mensagemPojo2.setCasaFinal(String.valueOf(casaDestino));
                    mensagemPojo2.setId_tabuleiro(id_tabuleiro);
                    mensagemPojo2.setTipo("movimento");
                    mensagemPojo2.setConteudo("movimento");
                    mensagemPojo2.setId_usuario_mecheu((String)request.getSession().getAttribute("id_usuario_sessao"));
                    
                    
                    chessCenterWS.mensagemPara(mensagemPojo1);
                    chessCenterWS.mensagemPara(mensagemPojo2);
                    
                }
                
             }
           
            
                                                
        }
        catch(Exception e)
        {
            resposta = "falha";
            System.out.println(e.getMessage());
        }
        finally 
        {    
            out.print(resposta);
            out.close();
        }
    }
    
    
    
   
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
