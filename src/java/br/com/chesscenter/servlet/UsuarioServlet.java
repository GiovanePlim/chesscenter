/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.chesscenter.servlet;

import br.com.chesscenter.dao.JogadorDAO;
import br.com.chesscenter.dao.PartidaDAO;
import br.com.chesscenter.pojo.JogadorPojo;
import br.com.chesscenter.pojo.MensagemPojo;
import br.com.chesscenter.pojo.PartidaPojo;
import br.com.chesscenter.websocket.ChessCenterWS;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author GIOVANE
 */
@WebServlet(name = "usuario", urlPatterns = {"/usuario"})
public class UsuarioServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        String resposta = "ok";
        String acao = request.getParameter("acao");
        

        try {
            
            JogadorDAO jogadorDAO = new JogadorDAO();
            
            if ( acao.equals("cadastrar") ){
                
                String email = request.getParameter("email");
                String login = request.getParameter("login");
                String senha = request.getParameter("senha");
                String nome = request.getParameter("nome");
               
                if ( jogadorDAO.verificaExisteEmail(email) )
                    resposta = "email_existente";
                else{
                    JogadorPojo jogadorPojo = new JogadorPojo();
                    
                    jogadorPojo.setEmail(email);
                    jogadorPojo.setSenha(senha);
                    jogadorPojo.setLogin(login);
                    jogadorPojo.setNome(nome);
                    
                    int id_usuario = jogadorDAO.inserirJogador(jogadorPojo);
                    
                    
                    //GRAVA OS DADOS NA SESSÃO
                    HttpSession sessao = request.getSession();
                    
                    sessao.setAttribute("id_usuario_sessao", id_usuario);
                    sessao.setAttribute("nome_usuario_sessao", nome);
                    sessao.setAttribute("email_usuario_sessao", email);
                    sessao.setAttribute("login_usuario_sessao", login);
                    
                }
                
                
            } else
                if ( acao.equals("login") ){
                    
                    String login = request.getParameter("login");
                    String senha = request.getParameter("senha");
                    
                    JogadorPojo jogadorPojo = new JogadorPojo();
                    
                    jogadorPojo.setLogin(login);
                    jogadorPojo.setSenha(senha);
                    
                    
                    jogadorPojo = jogadorDAO.buscarJogador(jogadorPojo);
                    
                    
                    if ( jogadorPojo != null ){
                        //GRAVA OS DADOS NA SESSÃO
                        HttpSession sessao = request.getSession();
                    
                        sessao.setAttribute("id_usuario_sessao", jogadorPojo.getId());
                        sessao.setAttribute("nome_usuario_sessao", jogadorPojo.getNome());
                        sessao.setAttribute("email_usuario_sessao", jogadorPojo.getEmail());
                        sessao.setAttribute("login_usuario_sessao", jogadorPojo.getLogin());
                    }else{
                        resposta = "nao";
                    }
                    
                }else
                if ( acao.equals("logof") ){
                    HttpSession sessao = request.getSession();
                    sessao.invalidate();
                    
                }else
                if ( acao.equals("atualiza_lista_status") ){
                    
                    ChessCenterWS ccws = new ChessCenterWS();
                    
                    ccws.atualiza_lista_status(request.getParameter("id_usuario"));
                    
                }else
                if ( acao.equals("convite_confronto") ){
                    
                   ChessCenterWS ccws = new ChessCenterWS();
                   MensagemPojo mesagemPojo = new MensagemPojo();
                   mesagemPojo.setId_destinatario(request.getParameter("id_adversario"));
                   mesagemPojo.setId_remetente((String) request.getSession().getAttribute("id_usuario_sessao"));
                   mesagemPojo.setTipo("convite");
                   mesagemPojo.setConteudo("confronto");
                   
                    ccws.mensagemPara(mesagemPojo);
                    
                }
                else
                if ( acao.equals("aceitar_convite_confronto") ){
                    
                    PartidaDAO partida = new PartidaDAO();
                    PartidaPojo  partidaPojo = new PartidaPojo();
                    
                    partidaPojo.setId_jogador_1(request.getParameter("id_adversario"));
                    partidaPojo.setId_jogador_2((String) request.getSession().getAttribute("id_usuario_sessao"));
                    
                    int id_partida = partida.inserirPartida(partidaPojo);
                    
                   ChessCenterWS ccws = new ChessCenterWS();
                   
                   MensagemPojo mesagemPojo = new MensagemPojo();
                   mesagemPojo.setId_destinatario(request.getParameter("id_adversario"));
                   mesagemPojo.setId_remetente((String) request.getSession().getAttribute("id_usuario_sessao"));
                   mesagemPojo.setId_partida(String.valueOf(id_partida));
                   mesagemPojo.setTipo("convite");
                   mesagemPojo.setCor("branco");
                   mesagemPojo.setConteudo("novo_confronto");
                   
                   ccws.mensagemPara(mesagemPojo);
                    
                   MensagemPojo mesagemPojo2 = new MensagemPojo();
                   
                   mesagemPojo2.setId_remetente(request.getParameter("id_adversario"));
                   mesagemPojo2.setId_destinatario((String) request.getSession().getAttribute("id_usuario_sessao"));
                   mesagemPojo2.setId_partida(String.valueOf(id_partida));
                   mesagemPojo2.setTipo("convite");
                   mesagemPojo2.setCor("preto");
                   mesagemPojo2.setConteudo("novo_confronto");
                   
                   ccws.mensagemPara(mesagemPojo2);
                   
                   
                    
                   
                   
                }
            
            
            
        } catch (Exception erro) {

        } finally {
            out.print(resposta);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
