/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.chesscenter.pojo;

import java.util.List;

/**
 *
 * @author GIOVANE
 */
public class PartidaPojo {
    
    private List<PartidaPojo> confrontos;
    
    private JogadorPojo jogador1;
    private JogadorPojo jogador2;
    private String idConfronto;
    private String status;
    private String id_jogador_1;
    private String id_jogador_2;
    
    public PartidaPojo(JogadorPojo jogador1, JogadorPojo jogador2){
        this.jogador1 = jogador1;
        this.jogador2 = jogador2;
    }
    
    public PartidaPojo(){
    }

    public List<PartidaPojo> getConfrontos() {
        return confrontos;
    }

    public void setConfrontos(List<PartidaPojo> confrontos) {
        this.confrontos = confrontos;
    }

    public JogadorPojo getJogador1() {
        return jogador1;
    }

    public void setJogador1(JogadorPojo jogador1) {
        this.jogador1 = jogador1;
    }

    public JogadorPojo getJogador2() {
        return jogador2;
    }

    public void setJogador2(JogadorPojo jogador2) {
        this.jogador2 = jogador2;
    }

    public String getIdConfronto() {
        return idConfronto;
    }

    public void setIdConfronto(String idConfronto) {
        this.idConfronto = idConfronto;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId_jogador_1() {
        return id_jogador_1;
    }

    public void setId_jogador_1(String id_jogador_1) {
        this.id_jogador_1 = id_jogador_1;
    }

    public String getId_jogador_2() {
        return id_jogador_2;
    }

    public void setId_jogador_2(String id_jogador_2) {
        this.id_jogador_2 = id_jogador_2;
    }
    
}
