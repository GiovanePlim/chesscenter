/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.chesscenter.pojo;

/**
 *
 * @author GIOVANE
 */
public class JogadorPojo {
    private String nome;
    private String login;
    private String senha;
    private String email;
    private String id;
    private String idSessao;
    private String idSessaoWS;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdSessao() {
        return idSessao;
    }

    public void setIdSessao(String idSessao) {
        this.idSessao = idSessao;
    }

    public String getIdSessaoWS() {
        return idSessaoWS;
    }

    public void setIdSessaoWS(String idSessaoWS) {
        this.idSessaoWS = idSessaoWS;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
}
