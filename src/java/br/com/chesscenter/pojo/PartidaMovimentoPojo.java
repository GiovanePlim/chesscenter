/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.chesscenter.pojo;

/**
 *
 * @author GIOVANE
 */
public class PartidaMovimentoPojo extends PartidaPojo{
    private String id_movimento;
    private String tabuleiro_pos_movimento;

    public String getId_movimento() {
        return id_movimento;
    }

    public void setId_movimento(String id_movimento) {
        this.id_movimento = id_movimento;
    }

    public String getTabuleiro_pos_movimento() {
        return tabuleiro_pos_movimento;
    }

    public void setTabuleiro_pos_movimento(String tabuleiro_pos_movimento) {
        this.tabuleiro_pos_movimento = tabuleiro_pos_movimento;
    }
}
