/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.chesscenter.pojo;

/**
 *
 * @author GIOVANE
 */
public class MensagemPojo {
    
    private String tipo;
    private String id_remetente;
    private String id_destinatario;
    private String id_partida;
    private String conteudo;
    private String cor;
    private String casaInicial;
    private String casaFinal;
    private String id_tabuleiro;
    private String id_usuario_mecheu;
    private String tipo_peca;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getId_remetente() {
        return id_remetente;
    }

    public void setId_remetente(String id_remetente) {
        this.id_remetente = id_remetente;
    }

    public String getId_destinatario() {
        return id_destinatario;
    }

    public void setId_destinatario(String id_destinatario) {
        this.id_destinatario = id_destinatario;
    }

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

    public String getId_partida() {
        return id_partida;
    }

    public void setId_partida(String id_partida) {
        this.id_partida = id_partida;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getCasaInicial() {
        return casaInicial;
    }

    public void setCasaInicial(String casaInicial) {
        this.casaInicial = casaInicial;
    }

    public String getCasaFinal() {
        return casaFinal;
    }

    public void setCasaFinal(String casaFinal) {
        this.casaFinal = casaFinal;
    }

    public String getId_tabuleiro() {
        return id_tabuleiro;
    }

    public void setId_tabuleiro(String id_tabuleiro) {
        this.id_tabuleiro = id_tabuleiro;
    }

    public String getId_usuario_mecheu() {
        return id_usuario_mecheu;
    }

    public void setId_usuario_mecheu(String id_usuario_mecheu) {
        this.id_usuario_mecheu = id_usuario_mecheu;
    }

    public String getTipo_peca() {
        return tipo_peca;
    }

    public void setTipo_peca(String tipo_peca) {
        this.tipo_peca = tipo_peca;
    }
    
    
}
