package br.com.chesscenter.conexao;

import java.sql.*;

public class ConexaoPostgres {
  private static Connection con;
  private static Statement stm;
  
  public static void abrir () throws Exception {
    String driver, url, usuario, senha;
    
    driver = "org.postgresql.Driver";
    url = "jdbc:postgresql://localhost:5432/xadrezweb";
    usuario = "postgres";
    senha = "postgres";
    
    Class.forName(driver);
    con = DriverManager.getConnection(url,usuario,senha);
    stm = con.createStatement();
  }
  
  public static void fechar () throws Exception {
    if (stm != null) stm.close();
    if (con != null) con.close();
  }
  
  public static void atualizar(String sql) throws Exception {
    if (con == null || stm == null) abrir();
    stm.execute(sql);
  }
  
  public static ResultSet selecionar(String sql) throws Exception {
    if (con == null || stm == null) abrir();
    return stm.executeQuery(sql);
  }
  
   public static int insere(String SQLQuery) throws Exception {

        try {
            
             if (con == null || stm == null) abrir();
             
                stm.executeUpdate(SQLQuery, Statement.RETURN_GENERATED_KEYS);

                ResultSet keys = stm.getGeneratedKeys();
                int id = 1;
                while (keys.next()) {
                    id = keys.getInt(1);
                }

            return id;
        } catch (Exception erro) {
            throw new Exception("Falha ao gravar registro no banco de dados: " + erro.getMessage());
        }
    }
}