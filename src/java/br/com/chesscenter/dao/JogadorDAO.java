/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.chesscenter.dao;

import br.com.chesscenter.conexao.ConexaoPostgres;
import java.io.Serializable;
import br.com.chesscenter.pojo.JogadorPojo;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author GIOVANE
 */
public class JogadorDAO implements Serializable{
    
   
    public int inserirJogador(JogadorPojo jogador)throws Exception{
        try {
            String SQL = "INSERT INTO tb_jogador (nome, login, senha, email, dt_cad, dt_alt ) VALUES ( '"+jogador.getNome()+"','"+jogador.getLogin()+"', '"+jogador.getSenha()+"', '"+jogador.getEmail()+"', now(), now()) ";
        
            return new ConexaoPostgres().insere(SQL);
                    
        } catch (Exception e) {
            throw new Exception("Erro ao salvar dados do jogador! : "+e.getMessage());
        }
    }
    
    public void alterarJogador(JogadorPojo jogador)throws Exception{
        try {
            String SQL = "UPDATE tb_jogador set nome = '"+jogador.getNome()+"', login = '"+jogador.getLogin()+"', senha = '"+jogador.getSenha()+"', email = '"+jogador.getEmail()+"',  dt_alt = now() WHERE id_jogador = "+jogador.getId()+" LIMIT 1 ";
        
             new ConexaoPostgres().atualizar(SQL);
                    
        } catch (Exception e) {
            throw new Exception("Erro ao alterar dados do jogador! : "+e.getMessage());
        }
    }
    
    public void registraIDSessaoWS(String id_sessao_ws, String id_jogador)throws Exception{
        try {
            String SQL = "UPDATE tb_jogador set id_sessao_ws = '"+id_sessao_ws+"' WHERE id_jogador = '"+id_jogador+"'  ";
        
            System.out.println("\n sql: "+SQL);
            
             new ConexaoPostgres().atualizar(SQL);
                    
        } catch (Exception e) {
            throw new Exception("Erro ao registrar sessao ws do jogador! : "+e.getMessage());
        }
    }
    
    public void deletarJogador(JogadorPojo jogador)throws Exception{
        try {
            String SQL = "UPDATE tb_jogador SET st_ativo = false,  dt_alt = now() WHERE id_jogador = "+jogador.getId()+" LIMIT 1 ";
        
             new ConexaoPostgres().atualizar(SQL);
                    
        } catch (Exception e) {
            throw new Exception("Erro ao alterar dados do jogador! : "+e.getMessage());
        }
    }
    
    public List<JogadorPojo>  buscarJogadores()throws Exception{
        try {
            String SQL = " SELECT id_jogador, id_sessao_ws, nome, login, senha, email, dt_cad, dt_alt FROM tb_jogador WHERE st_ativo ";
        
            ResultSet jogadores = new ConexaoPostgres().selecionar(SQL);
            
            List<JogadorPojo> lista_de_jogadores = new ArrayList();
            
            while ( jogadores.next() ){
                JogadorPojo jogadorPojoTemp = new JogadorPojo();
                
                jogadorPojoTemp.setId(jogadores.getString("id_jogador"));
                jogadorPojoTemp.setIdSessaoWS(jogadores.getString("id_sessao_ws"));
                jogadorPojoTemp.setNome(jogadores.getString("nome"));
                jogadorPojoTemp.setLogin(jogadores.getString("login"));
                jogadorPojoTemp.setSenha(jogadores.getString("senha"));
                jogadorPojoTemp.setEmail(jogadores.getString("email"));
                
                
                lista_de_jogadores.add(jogadorPojoTemp);
            }
            
            return lista_de_jogadores;
                    
        } catch (Exception e) {
            throw new Exception("Erro buscar lista de jogadores! : "+e.getMessage());
        }
    }
    
    public JogadorPojo buscarJogador(JogadorPojo jogadorPojo)throws Exception{
        try {
            String SQL = "SELECT id_jogador, id_sessao_ws, nome, login, senha, email, dt_cad, dt_alt FROM tb_jogador WHERE st_ativo and senha = '"+jogadorPojo.getSenha()+"' and ( login = '"+jogadorPojo.getLogin()+"'  or email = '"+jogadorPojo.getLogin()+"' ) LIMIT 1 ";
        
             ResultSet jogador =  new ConexaoPostgres().selecionar(SQL);
             
             
             if ( jogador.next() ){
                jogadorPojo.setId(jogador.getString("id_jogador"));
                jogadorPojo.setIdSessaoWS(jogador.getString("id_sessao_ws"));
                jogadorPojo.setNome(jogador.getString("nome"));
                jogadorPojo.setLogin(jogador.getString("login"));
                jogadorPojo.setSenha(jogador.getString("senha"));
                jogadorPojo.setEmail(jogador.getString("email"));
             }else{
                 return null;
             }
             
             
             return jogadorPojo;
                 
                    
        } catch (Exception e) {
            throw new Exception("Erro ao buscar dados do jogador! : "+e.getMessage());
        }
    }
    
    public boolean verificaExisteEmail(String email)throws Exception{
        try {
            String SQL = "SELECT email FROM tb_jogador WHERE st_ativo and email = '"+email+"' LIMIT 1";
        
             ResultSet jogador =  new ConexaoPostgres().selecionar(SQL);
             
             if ( jogador.next() ){
                 return true;
             }
             return false;
                 
                    
        } catch (Exception e) {
            throw new Exception("Erro ao verificar se email existe : "+e.getMessage());
        }
    }
    
    
}
