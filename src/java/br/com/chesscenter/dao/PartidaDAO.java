/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.chesscenter.dao;

import br.com.chesscenter.conexao.ConexaoPostgres;
import br.com.chesscenter.pojo.PartidaPojo;
import java.io.Serializable;

/**
 *
 * @author GIOVANE
 */
public class PartidaDAO implements Serializable{
    
    public int inserirPartida(PartidaPojo partidaPojo)throws Exception{
        try {
            String SQL = "INSERT INTO tb_confronto (id_jogador_1, id_jogador_2, dt_alt, dt_cad) VALUES ( '"+partidaPojo.getId_jogador_1()+"','"+partidaPojo.getId_jogador_2()+"', now(), now()) ";
        
            return new ConexaoPostgres().insere(SQL);
                    
        } catch (Exception e) {
            throw new Exception("Erro ao iniciar confronto! : "+e.getMessage());
        }
    }
    
    
}
