<%-- 
    Document   : modais
    Created on : 14/06/2015, 14:09:38
    Author     : GIOVANE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- CSS do plugin -->
<link rel="stylesheet" href="plugins/reveal/reveal.css">	
<!-- Biblioteca jQuery -->
<script type="text/javascript" src="plugins/reveal/jquery-1.4.4.min.js">
</script> 
<!-- JS do plugin -->
<script type="text/javascript" src="plugins/reveal/jquery.reveal.js"></script>


<div id="menu_adversario" class="reveal-modal">
    <input type="hidden" id="id_adversario">
    <h4>Menu de opções do adversário</h4>
    <div class="btn-group" role="group" aria-label="...">
        <button type="button" class="btn btn-default glyphicon glyphicon-knight" onclick="proporConfronto($('#id_adversario').val())" ><br>VS<span id="nome_adversario"></span><br>Propor partida</button>
    </div>
    <a class="close-reveal-modal" id="fechar_menu_adversario">×</a>
</div>


<div id="convite_partida" class="reveal-modal">
    <input type="hidden" id="id_adversario_convidou">
    <h4>Convite para confronto</h4>
    <span id="nome_adversario_convidou"></span> te convidou para jogar <br>
    <div class="btn-group glyphicon" role="group" aria-label="...">
        <button type="button" class="btn btn-default glyphicon glyphicon-thumbs-up" onclick="aceitarConfronto($('#id_adversario_convidou').val())" ><br>Aceitar</button>
        <button type="button" class="btn btn-default glyphicon glyphicon-thumbs-down" onclick="recusarConfronto()" ><br>Recusar</button>
    </div>
    <a class="close-reveal-modal" id="fechar_menu_confronto">×</a>
</div>

<div id="peao_promovido" class="reveal-modal">
    <input type="hidden" id="id_casa_destino">
    <input type="hidden" id="id_casa_inicial">
    <input type="hidden" id="id_adversario_promocao">
    <input type="hidden" id="cor">
    <input type="hidden" id="id_tabuleiro">
    <h4>Promoção de peao</h4>
    <span>Escolha uma das pecas</span>  <br>
    <div class="btn-group glyphicon" role="group" aria-label="...">
        <button  type="button" id="btn_peca_1" class="btn btn-default glyphicon botao_customizado" onclick="promove('cavalo',$('#id_casa_inicial').val(),$('#id_casa_destino').val(), $('#id_adversario_promocao').val(), $('#id_tabuleiro').val(), $('#cor').val())" ></button>
        <button  type="button" id="btn_peca_2" class="btn btn-default glyphicon botao_customizado" onclick="promove('bispo',$('#id_casa_inicial').val(),$('#id_casa_destino').val(), $('#id_adversario_promocao').val(), $('#id_tabuleiro').val(), $('#cor').val())" ></button>
        <button  type="button" id="btn_peca_3" class="btn btn-default glyphicon botao_customizado" onclick="promove('torre',$('#id_casa_inicial').val(),$('#id_casa_destino').val(), $('#id_adversario_promocao').val(), $('#id_tabuleiro').val(), $('#cor').val())" ></button>
        <button  type="button" id="btn_peca_4" class="btn btn-default glyphicon botao_customizado" onclick="promove('dama',$('#id_casa_inicial').val(),$('#id_casa_destino').val(), $('#id_adversario_promocao').val(), $('#id_tabuleiro').val(), $('#cor').val())" ></button>
    </div>
    <a class="close-reveal-modal" id="fechar_menu_promocao">×</a>
</div>	

