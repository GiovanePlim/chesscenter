/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var diagonal = [];

diagonal[0] = [62, 55];
diagonal[1] = [60, 53, 46, 39];
diagonal[2] = [58, 51, 44, 37, 30, 23];
diagonal[3] = [56, 49, 42, 35, 28, 21, 14, 7];
diagonal[4] = [40, 33, 26, 19, 12, 5];
diagonal[5] = [24, 17, 10, 3];
diagonal[6] = [8, 1];
diagonal[7] = [40, 49, 58];
diagonal[8] = [24, 33, 42, 51, 60];
diagonal[9] = [8, 17, 26, 35, 44, 53, 62];
diagonal[10] = [1, 10, 19, 28, 37, 46, 55];
diagonal[11] = [3, 12, 21, 30, 39];
diagonal[12] = [5, 14, 23];
diagonal[13] = [48, 57];
diagonal[14] = [32, 41, 50, 59];
diagonal[15] = [16, 25, 34, 43, 52, 61];
diagonal[16] = [0, 9, 18, 27, 36, 45, 54, 63];
diagonal[17] = [2, 11, 20, 29, 38, 47];
diagonal[18] = [4, 13, 22, 31];
diagonal[19] = [6, 15];
diagonal[20] = [61, 54, 47];
diagonal[21] = [59, 52, 45, 38, 31];
diagonal[22] = [57, 50, 43, 36, 29, 22, 15];
diagonal[23] = [48, 41, 34, 27, 20, 13, 6];
diagonal[24] = [32, 25, 18, 11, 4];
diagonal[25] = [16, 9, 2];

var coluna = [];

coluna[0] = [0, 8, 16, 24, 32, 40, 48, 56];
coluna[1] = [1, 9, 17, 25, 33, 41, 49, 57];
coluna[2] = [2, 10, 18, 26, 34, 42, 50, 58];
coluna[3] = [3, 11, 19, 27, 35, 43, 51, 59];
coluna[4] = [4, 12, 20, 28, 36, 44, 52, 60];
coluna[5] = [5, 13, 21, 29, 37, 45, 53, 61];
coluna[6] = [6, 14, 22, 30, 38, 46, 54, 62];
coluna[7] = [7, 15, 23, 31, 39, 47, 55, 63];

var linha = [];

linha[0] = [0, 1, 2, 3, 4, 5, 6, 7];
linha[1] = [8, 9, 10, 11, 12, 13, 14, 15];
linha[2] = [16, 17, 18, 19, 20, 21, 22, 23];
linha[3] = [24, 25, 26, 27, 28, 29, 30, 31];
linha[4] = [32, 33, 34, 35, 36, 37, 38, 39];
linha[5] = [40, 41, 42, 43, 44, 45, 46, 47];
linha[6] = [48, 49, 50, 51, 52, 53, 54, 55];
linha[7] = [56, 57, 58, 59, 60, 61, 62, 63];


window.onload = function () {

    //$("#altura").text($( window ).height()) ;
     var altura_janela = $(window).width();

    $('#tam_pag').html(altura_janela+"px");

}



window.onresize = function () {

    var altura_janela = $(window).width();

    $('#tam_pag').html(altura_janela+"px");

//    $('#tabuleiros').height(altura_janela - $('#header').height());
//
//    if (altura_janela <= 378)
//        $('#tabuleiros').height(200);

}

var bispo_branco = '<img cor="branco" peca="bispo" src="img/lg-Chess-tile-Bishop-1.png" class="peca"  alt="Bispo branco" >';
var rei_branco = '<img cor="branco" permite_roque="sim"  peca="rei" src="img/lg-Chess-tile-King-3.png" class="peca" alt="Rei branco" >';
var peao_branco = '<img cor="branco" peca="peao" sofre_enpassant="nao" src="img/lg-Chess-tile-Pawn-3.png" class="peao peca" alt="Peao branco" >';
var dama_branca = '<img cor="branco" peca="dama" src="img/lg-Chess-tile-Queen-3.png" class="peca" alt="Dama branca" >';
var cavalo_branco = '<img cor="branco" peca="cavalo" src="img/lg-chess-horse.png"  class="peca" alt="Cavalo branco" >';
var torre_branca = '<img cor="branco" permite_roque="sim" peca="torre" src="img/lg-Chess-tile-Rook-3.png" class="peca" alt="Torre branca" >';

var bispo_negro = '<img cor="preto" peca="bispo" src="img/918457-lg-bishop-chess-piece.png" class="peca" alt="Bispo negro" >';
var rei_negro = '<img cor="preto" permite_roque="sim" peca="rei" src="img/lg-Chess-tile-King-1.png"  class="peca" alt="Rei negro" >';
var peao_negro = '<img cor="preto" peca="peao"  sofre_enpassant="nao" src="img/lg-Chess-tile-Pawn-1.png" alt="Peao negro" class="peao peca" >';
var dama_negra = '<img cor="preto" peca="dama" src="img/lg-Chess-tile-Queen-1.png"  class="peca" alt="Dama negra" >';
var cavalo_negro = '<img cor="preto" peca="cavalo" src="img/lg-Chess-tile-Knight-3.png" class="peca"  alt="Cavalo negro" >';
var torre_negra = '<img cor="preto" permite_roque="sim" peca="torre"  src="img/lg-Chess-tile-Rook-1.png" class="peca"  alt="Torre negra" >';


var selecionado = "";



function seleciona(elemento) {
    
    //alert(elemento);

    var id_tabuleiro = jQuery("#"+elemento.id).parent().attr("id")

    var minha_cor = jQuery("#" + id_tabuleiro + " [name='cor']").val();
    var ultimo_a_jogar = jQuery("#" + id_tabuleiro + " [name='ultimo_a_jogar']").val();



    //jQuery("#" + id_tabuleiro + " [name='casa']").removeClass("marca_movimento");
    var casa = elemento.id.split("-")[1];

    console.log("Casa: " + casa);

    aplicaSelecao(id_tabuleiro, casa);


    marcaMovimentoPossivel(elemento, casa, id_tabuleiro, minha_cor);

    if (minha_cor != ultimo_a_jogar) {

        var elemento_text = jQuery("#" + elemento.id).html();

        if (elemento_text != "") {

            var cor = elemento.getElementsByTagName("img")[0].getAttribute("cor");

            if (selecionado == "") {
                if (cor == minha_cor) {
                    selecionado = elemento;
                }

            } else {
                if (cor != minha_cor) {

                    if (jQuery("#" + elemento.id).hasClass("marca_movimento"))
                        movimenta(id_tabuleiro, selecionado, elemento);

                } else {
                    selecionado = elemento;
                }
            }

        } else {
            if (selecionado != "") {
                if (jQuery("#" + elemento.id).hasClass("marca_movimento"))
                    movimenta(id_tabuleiro, selecionado, elemento);
                else
                    jQuery("#" + id_tabuleiro + " [name='casa']").removeClass("marca_movimento");

            }


        }

    }
    else
        jQuery("#" + id_tabuleiro + " [name='casa']").removeClass("marca_movimento");

}

function marcaMovimentoPossivel(elemento, casa, id_tabuleiro, minha_cor){
    var img = elemento.getElementsByTagName("img")[0];

    if (img != undefined) {


        var peca = img.getAttribute("peca");
        var cor = img.getAttribute("cor");

        if (cor == minha_cor) {

            if (peca != undefined) {

                jQuery("#" + id_tabuleiro + " [name='casa']").removeClass("marca_movimento");

                switch (peca) {
                    case  'peao':
                        marcaMovimentoPeao(casa, id_tabuleiro);
                        break;
                    case 'torre':
                        marcaLinhaColunaDiagonal(casa, id_tabuleiro, linha, 'linha');
                        marcaLinhaColunaDiagonal(casa, id_tabuleiro, coluna, 'coluna');
                        break;
                    case 'bispo':
                        marcaLinhaColunaDiagonal(casa, id_tabuleiro, diagonal, 'diagonal');
                        break;
                    case 'dama':
                        marcaLinhaColunaDiagonal(casa, id_tabuleiro, linha, 'linha');
                        marcaLinhaColunaDiagonal(casa, id_tabuleiro, coluna, 'coluna');
                        marcaLinhaColunaDiagonal(casa, id_tabuleiro, diagonal, 'diagonal');
                        break;
                    case 'rei':
                        marcaLinhaColunaDiagonalUmaCasa(casa, id_tabuleiro, linha, 'linha');
                        marcaLinhaColunaDiagonalUmaCasa(casa, id_tabuleiro, coluna, 'coluna');
                        marcaLinhaColunaDiagonalUmaCasa(casa, id_tabuleiro, diagonal, 'diagonal');
                        break;
                    case 'cavalo':
                        marcaMovimentoL(casa, id_tabuleiro);
                        break;

                }

            }

        }
    }
}


function movimenta(id_tabuleiro, elemento, elemento2) {

    var cor = jQuery("#" + id_tabuleiro + " [name='cor']").val();
    var idAdversario = jQuery("#" + id_tabuleiro + " [name='id_adversario']").val();
    var tabululeiro = atualizaTabuleiro(id_tabuleiro);
    var casaInicial = elemento.id.split("-")[1];
    var casaDestino = elemento2.id.split("-")[1];
    var tipo = elemento.getElementsByTagName("img")[0].getAttribute("peca");


    //enviaMovimento(id_destinatario, id_tabuleiro, casaInicial, casaFinal, cor, id_tabuleiro)

    var posicao_linha = -1;

    for (var i = 0; i < coluna.length; i++) {
        posicao_linha = esta(parseInt(casaDestino), coluna[i]);

        if (posicao_linha > -1)
            break;

    }

    if (tipo == "peao" && (posicao_linha == 0 || posicao_linha == 7)) {

        if (cor == "branco") {

            $("#btn_peca_1").html(cavalo_branco);
            $("#btn_peca_2").html(bispo_branco);
            $("#btn_peca_3").html(torre_branca);
            $("#btn_peca_4").html(dama_branca);

        } else {

            $("#btn_peca_1").html(cavalo_negro);
            $("#btn_peca_2").html(bispo_negro);
            $("#btn_peca_3").html(torre_negra);
            $("#btn_peca_4").html(dama_negra);


        }


        $("#cor").val(cor);
        $("#id_casa_inicial").val(casaInicial);
        $("#id_adversario_promocao").val(idAdversario);
        $("#id_tabuleiro").val(id_tabuleiro);
        $("#id_casa_destino").val(casaDestino);

        $("#peao_promovido").reveal();


    }
    else
    {

        enviaMovimento(idAdversario, id_tabuleiro, casaInicial, casaDestino, cor, id_tabuleiro.split("tab_")[1], "movimento", "");
    }






//    var enpassant = "nao";
//    
//    
//    var request = $.ajax({
//        url: "ConfrontoServlet",
//        method: "GET",
//        data: {adversario: idAdversario, acao: "movimenta", cor: cor, tipoPeca: tipo, casaInicial: casaInicial, casaDestino: casaDestino, tabuleiro: tabululeiro, id_tabuleiro: id_tabuleiro},
//        dataType: "text",
//        success: function (retorno) {
//            
//        }
//
//    });



}

window.onload = function(){
    criaTabuleiroConfronto('2', '2', 'branco')
} 

//PARA CADA CONFRONTO, SEU PROPRIO TABULEIRO
function criaTabuleiroConfronto(idConfronto, id_adversario, cor) {

    var tab = "<div class='tabuleiro' style='display: none; name='tabuleiros'  id='tab_" + idConfronto + "' >";


    tab += "<input type='hidden' name='id_adversario' value='" + id_adversario + "'>";
    tab += "<input type='hidden' name='cor' value='" + cor + "'>";
    tab += "<input type='hidden' name='ultimo_a_jogar' value='preto' >";

    var impar = false;

    for (var i = 0; i < 64; ++i) {
        if (!impar) {
            tab += "<div name='casa' class='casa branca'   onclick=\"seleciona(this)\"></div>";
        }
        else {
            tab += "<div name='casa' class='casa escura' onclick=\"seleciona(this)\"></div>";
        }

        impar = !impar;

        if ((i + 1) % 8 == 0)
            impar = !impar;
        else
            impar = impar;

    }

    tab += "</div>";


    $("#tabuleiros").append(tab);
    
    //$("#letras_").append("<div class='letra'>A</div><div class='letra'>B</div><div class='letra'>C</div><div class='letra'>D</div><div class='letra'>E</div><div class='letra'>F</div><div class='letra'>G</div>");
    //$("#ul_tabs").append("<li><a href='#tab_" + idConfronto + "' onclick=\"mostraTabuleiro('tab_"+ idConfronto+"')\"> VS "+$("#jogador_"+id_adversario).text()+"</a></li>");
    
    if (cor == "branco") {
        escreveTabuleiro("tab_" + idConfronto, 'brancas');
    } else {
        escreveTabuleiro("tab_" + idConfronto, '');
    }


    jQuery("#conf_" + id_adversario).val("tab_" + idConfronto);

    mostraTabuleiro("tab_" + idConfronto);
    
    //tabs();
    
    
    //addTab($("#jogador_"+id_adversario).text(), "tab_" + idConfronto)
    
    

}

function adicionaAbaJogo(jogador, id_tabuleiro){
    
}


function mostraTabuleiro(id_tabuleiro) {

    var tabuleiros = jQuery("[name='tabuleiros']");

    //esconde os tabueleiros
    for (var i = 0; i < tabuleiros.length; i++) {
        jQuery("#" + tabuleiros[i].id).hide();
    }
    //mostra o tabuleiro selecionado
    jQuery("#" + id_tabuleiro).show();
    
    //tabs();

}


function escreveTabuleiro(id_tabuleiro, cor_pecas) {

    var casas = jQuery("#" + id_tabuleiro + " [name='casa']");


    if (cor_pecas != 'brancas') {

        for (var i = 0; i < casas.length; i++) {
            casas[i].id = (id_tabuleiro + "-" + (63 - i));
        }

        casas[0].innerHTML = torre_branca;
        casas[1].innerHTML = cavalo_branco;
        casas[2].innerHTML = bispo_branco;
        casas[3].innerHTML = rei_branco;
        casas[4].innerHTML = dama_branca;
        casas[5].innerHTML = bispo_branco;
        casas[6].innerHTML = cavalo_branco;
        casas[7].innerHTML = torre_branca;

        for (var i = 8; i < 16; i++) {
            casas[i].innerHTML = peao_branco;
        }

        casas[56].innerHTML = torre_negra;
        casas[57].innerHTML = cavalo_negro;
        casas[58].innerHTML = bispo_negro;
        casas[59].innerHTML = rei_negro;
        casas[60].innerHTML = dama_negra;
        casas[61].innerHTML = bispo_negro;
        casas[62].innerHTML = cavalo_negro;
        casas[63].innerHTML = torre_negra;

        for (var i = 48; i < 56; i++) {
            casas[i].innerHTML = peao_negro;
        }
    } else {

        for (var i = 0; i < casas.length; i++) {
            casas[i].id = (id_tabuleiro + "-" + i);
        }

        casas[0].innerHTML = torre_negra;
        casas[1].innerHTML = cavalo_negro;
        casas[2].innerHTML = bispo_negro;
        casas[3].innerHTML = dama_negra;
        casas[4].innerHTML = rei_negro;
        casas[5].innerHTML = bispo_negro;
        casas[6].innerHTML = cavalo_negro;
        casas[7].innerHTML = torre_negra;

        for (var i = 8; i < 16; i++) {
            casas[i].innerHTML = peao_negro;
        }

        casas[56].innerHTML = torre_branca;
        casas[57].innerHTML = cavalo_branco;
        casas[58].innerHTML = bispo_branco;
        casas[59].innerHTML = dama_branca;
        casas[60].innerHTML = rei_branco;
        casas[61].innerHTML = bispo_branco;
        casas[62].innerHTML = cavalo_branco;
        casas[63].innerHTML = torre_branca;

        for (var i = 48; i < 56; i++) {
            casas[i].innerHTML = peao_branco;
        }


    }

}


function atualizaTabuleiro(id_tabuleiro) {

    var tabuleiro = [];

    var casas = jQuery("#" + id_tabuleiro + " [name='casa']");
    var cor = jQuery("#" + id_tabuleiro + " [name='cor']").val();


    //SE O TABULEIRO NÃO ESTÁ VIRADO
    if (cor != 'preto') {

        for (var i = 0; i < casas.length; i++) {
            if (casas[i].innerHTML != "")
                tabuleiro[i] = casas[i].getElementsByTagName("img")[0].getAttribute("cor");
            else
                tabuleiro[i] = "";
        }

    } else {

        for (var i = 0; i < casas.length; i++) {
            if (casas[i].innerHTML != "")
                tabuleiro[(63 - i)] = casas[i].getElementsByTagName("img")[0].getAttribute("cor");
            else
                tabuleiro[(63 - i)] = "";
        }
    }

    return tabuleiro.toString();
}

function movePeca(id_adversario, id_tabuleiro, casaInicial, casaDestino, cor, id_usuario_mecheu, tipo_movimento, tipo_peca) {

    //se o tabuleiro nao enstiver visivel, ira piscar o o nome do usuario para avisar sobre o movimento
    if (id_usuario_mecheu != id_usuario) {
        if ($('#' + id_tabuleiro).is(':visible')) {

        } else {
            inicia_pisca(id_adversario);
        }
    }

    jQuery("#" + id_tabuleiro + " [name='ultimo_a_jogar']").val(cor);

    //armazena a casa capturada para posteriormente verificar se e uma peca capturada ou vazia para por exemplo, verificar se é um movimento empassant
    var casaCapturada = jQuery("#" + id_tabuleiro + "-" + casaDestino).html();

    jQuery("#" + id_tabuleiro + "-" + casaDestino).html(jQuery("#" + id_tabuleiro + "-" + casaInicial).html());
    jQuery("#" + id_tabuleiro + "-" + casaInicial).html("");
    //aplicaSelecaoMovimento(id_tabuleiro, casaInicial, casaDestino);


    //porque o empassante so e possivel no mesmo lance
    jQuery("#" + id_tabuleiro + " div img[peca='peao']").attr("sofre_enpassant", "nao");


    //verifica se o movimento foi de um peao, e se moveu duas casas para mudar o atributo que permite a captura empassant
    var tipo = jQuery("#" + id_tabuleiro + "-" + casaDestino + " img").attr("peca");

    //console.log("tipo: " + tipo);


    switch (tipo) {
        case "peao":
            var casa1 = parseInt(casaInicial);
            var casa2 = parseInt(casaDestino);


            if (cor == "branco") {

                //se a distancia das casas for igual a 16
                //quer dizer que se mecheu duas casa, porta esta sugeito a sofrer a captura em passant    
                if (casa1 - casa2 == 16) {
                    jQuery("#" + id_tabuleiro + "-" + casaDestino + " img").attr("sofre_enpassant", "sim");
                } else
                //se foi um movimento em diagonal
                if (casa1 - casa2 == 7 || casa1 - casa2 == 9)
                {
                    //se nao houve captura na casa final se trata do movimento empassant
                    if (casaCapturada == "")
                        jQuery("#" + id_tabuleiro + "-" + (casa2 + 8)).html("");
                }
                if (tipo_movimento == "promocao") {
                    switch (tipo_peca) {
                        case 'dama':
                            jQuery("#" + id_tabuleiro + "-" + casaDestino).html(dama_branca);
                            break;
                        case 'cavalo':
                            jQuery("#" + id_tabuleiro + "-" + casaDestino).html(cavalo_branco);
                            break;
                        case 'bispo':
                            jQuery("#" + id_tabuleiro + "-" + casaDestino).html(bispo_branco);
                            break;
                        case 'torre':
                            jQuery("#" + id_tabuleiro + "-" + casaDestino).html(torre_branca);
                            break;
                    }
                }

            } else {
                if (casa2 - casa1 == 16) {
                    jQuery("#" + id_tabuleiro + "-" + casaDestino + " img").attr("sofre_enpassant", "sim");
                } else
                //se foi um movimento em diagonal
                if (casa2 - casa1 == 7 || casa2 - casa1 == 9)
                {
                    //se nao houve captura na casa final se trata do movimento empassant
                    if (casaCapturada == "")
                        jQuery("#" + id_tabuleiro + "-" + (casa2 - 8)).html("");
                }

                if (tipo_movimento == "promocao") {
                    switch (tipo_peca) {
                        case 'dama':
                            jQuery("#" + id_tabuleiro + "-" + casaDestino).html(dama_negra);
                            break;
                        case 'cavalo':
                            jQuery("#" + id_tabuleiro + "-" + casaDestino).html(cavalo_negro);
                            break;
                        case 'bispo':
                            jQuery("#" + id_tabuleiro + "-" + casaDestino).html(bispo_negro);
                            break;
                        case 'torre':
                            jQuery("#" + id_tabuleiro + "-" + casaDestino).html(torre_negra);
                            break;
                    }
                }

            }
            break;
        case "torre":
            jQuery("#" + id_tabuleiro + "-" + casaDestino + " img").attr("permite_roque", "nao");
            break;

        case "rei":

            var casaMaior;
            var casaMenor;
            //CASO SEJA UM ROQUE, ARMAZENARA O TIPO DELE
            var tr = "";

            if (parseInt(casaInicial) > parseInt(casaDestino))
            {
                tr = "maior";
                casaMaior = parseInt(casaInicial);
                casaMenor = parseInt(casaDestino);
            } else {
                tr = "menor";
                casaMenor = parseInt(casaInicial);
                casaMaior = parseInt(casaDestino);
            }

            //SE MOVEU 2 CASAS E UM ROQUE
            if ((casaMaior - casaMenor) == 2) {

                if (tr == "maior") {
                    jQuery("#" + id_tabuleiro + "-" + (casaMenor + 1)).html(jQuery("#" + id_tabuleiro + "-" + (casaMenor - 2)).html());
                    jQuery("#" + id_tabuleiro + "-" + (casaMenor - 2)).html("");
                } else {
                    jQuery("#" + id_tabuleiro + "-" + (casaMaior - 1)).html(jQuery("#" + id_tabuleiro + "-" + (casaMaior + 1)).html());
                    jQuery("#" + id_tabuleiro + "-" + (casaMaior + 1)).html("");
                }

            }

            //COMO O REI MECHEU, NAO SE PODE MAIS FAZER O ROQUE
            jQuery("#" + id_tabuleiro + " div img[peca='torre'][cor='" + cor + "']").attr("permite_roque", "nao");
            jQuery("#" + id_tabuleiro + "-" + casaDestino + " img").attr("permite_roque", "nao");
            break;



    }

    jQuery("#" + id_tabuleiro + " [name='casa']").removeClass("marca_movimento");

    var objCheque = reiEmCheque(id_tabuleiro, "", "", "");
    if (objCheque.cheque > -1 && jQuery("#" + id_tabuleiro + " [name='cor']").val() != cor){
        if (reiEmChequeMate(id_tabuleiro).cheque_mate)
            alert("cheque mate!!");
        else
            alert("chque!!");
    }else
        if (reiEmChequeMate(id_tabuleiro).cheque_mate)
            alert("Empate!!");
        




}

function aplicaSelecao(id_tabuleiro, id_casa) {
    jQuery("#" + id_tabuleiro + " [name='casa']").removeClass("selecionada");
    jQuery("#" + id_tabuleiro + "-" + id_casa).addClass("selecionada");

}

function aplicaSelecaoMovimento(id_tabuleiro, id_casa, id_casa_final) {
    jQuery("#" + id_tabuleiro + " [name='casa']").removeClass("selecionada");
    jQuery("#" + id_tabuleiro + "-" + id_casa).addClass("selecionada");
    jQuery("#" + id_tabuleiro + "-" + id_casa_final).addClass("selecionada");

}

function som(som) {
    $('<audio id="som"><source src="notify.ogg" type="audio/ogg"><source src="notify.mp3" type="audio/mpeg"></audio>').appendTo('body');

// play sound
    $('#som')[0].play();

}


function marcaLinhaColunaDiagonal(casa, id_tabuleiro, array_linha_coluna_diagonal, tipo) {

    //cor das pecas do jogador
    var cor = jQuery("#" + id_tabuleiro + " [name='cor']").val();

    //ira armazena as posicoes das duas diagonais quem cruzam a casa
    var armazena_posicao_linha_coluna_diagonal = [];


    for (var i = 0; i < array_linha_coluna_diagonal.length; i++) {

        //retorna a posicao da casa na linha_coluna, ou -1 se nao encontrar nada
        var x = esta(casa, array_linha_coluna_diagonal[i]);

        if (x > -1) {

            var obj_linha_coluna_diagonal = {
                posicao_casa: x,
                posicao_linha_coluna_diagonal: i
            }
            armazena_posicao_linha_coluna_diagonal.push(obj_linha_coluna_diagonal);
            chama(armazena_posicao_linha_coluna_diagonal, casa);

        }

//        if (armazena_posicao_linha_coluna_diagonal.length > 1 || ((tipo == 'diagonal') && (casa == 0 || casa == 56 || casa == 63 || casa == 07)))
//            break;

    }

    function chama(armazena_posicao_linha_coluna_diagonal, casa) {
        //se encontrou as duas digonais ou clicou em uma casa que esta na linha_coluna maior
        if ((armazena_posicao_linha_coluna_diagonal.length > 1) || ((tipo == 'diagonal') && (casa == 0 || casa == 56 || casa == 63 || casa == 07)) || (armazena_posicao_linha_coluna_diagonal.length > 0 && (tipo == 'linha' || tipo == 'coluna')))
        {
            //chama a funcao que faz a selecao das diagonais
            faz(armazena_posicao_linha_coluna_diagonal);
        }
    }

    function faz(armazena_posicao_linha_coluna) {

        for (var i = 0; i < armazena_posicao_linha_coluna.length; i++) {

            //posicao do vetor da linha_coluna  encontrado antes de chamar a funcao
            var l_linha_coluna_diagonal = armazena_posicao_linha_coluna[i].posicao_linha_coluna_diagonal;
            var posicao_casa = armazena_posicao_linha_coluna[i].posicao_casa;

            //marca de uma poiscao alem da casa até o final da linha_coluna
            for (var x = posicao_casa + 1; x < array_linha_coluna_diagonal[l_linha_coluna_diagonal].length; x++) {

                //verifica se a casa não esta vazia
                if (jQuery("#" + id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).html() != "")
                {
                    var p_cor = document.getElementById(id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).getElementsByTagName("img")[0].getAttribute("cor");

                    //se for uma peca minha, para
                    if (p_cor == cor)
                        break;
                    else {
                        //se for do adversario, marca e depois para
                        
                        if (reiEmCheque(id_tabuleiro, array_linha_coluna_diagonal[l_linha_coluna_diagonal][x], "", parseInt(casa)).cheque == -1 )
                            jQuery("#" + id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).addClass("marca_movimento");
                        
                        break;
                    }


                } else {
                    if (reiEmCheque(id_tabuleiro, array_linha_coluna_diagonal[l_linha_coluna_diagonal][x], "", parseInt(casa)).cheque == -1 )
                        jQuery("#" + id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).addClass("marca_movimento");

                }


            }

            //marca de de uma posicao antes da casa até o inicio da linha_coluna
            for (var x = posicao_casa - 1; x >= 0; x--) {

                //verifica se a casa não esta vazia
                if (jQuery("#" + id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).html() != "")
                {

                    var p_cor = document.getElementById(id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).getElementsByTagName("img")[0].getAttribute("cor");

                    //se for uma peca minha, para
                    if (p_cor == cor)
                        break;
                    else {
                        //se for do adversario, marca e depois para
                        if (reiEmCheque(id_tabuleiro, array_linha_coluna_diagonal[l_linha_coluna_diagonal][x], "", parseInt(casa)).cheque == -1 )
                            jQuery("#" + id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).addClass("marca_movimento");
                        break;
                    }

                }
                else {

                    //se nao apenas seleciona a casa
                    if (reiEmCheque(id_tabuleiro, array_linha_coluna_diagonal[l_linha_coluna_diagonal][x], "", parseInt(casa)).cheque == -1 )
                        jQuery("#" + id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).addClass("marca_movimento");


                }

            }
//
        }
    }

}

function marcaLinhaColunaDiagonalUmaCasa(casa, id_tabuleiro, array_linha_coluna_diagonal, tipo) {

    //cor das pecas do jogador
    var cor = jQuery("#" + id_tabuleiro + " [name='cor']").val();

    //ira armazena as posicoes das duas diagonais quem cruzam a casa
    var armazena_posicao_linha_coluna_diagonal = [];


    for (var i = 0; i < array_linha_coluna_diagonal.length; i++) {

        //retorna a posicao da casa na linha_coluna, ou -1 se nao encontrar nada
        var x = esta(casa, array_linha_coluna_diagonal[i]);

        if (x > -1) {

            var obj_linha_coluna_diagonal = {
                posicao_casa: x,
                posicao_linha_coluna_diagonal: i
            }
            armazena_posicao_linha_coluna_diagonal.push(obj_linha_coluna_diagonal);
            chama(armazena_posicao_linha_coluna_diagonal, casa);

        }

//        if (armazena_posicao_linha_coluna_diagonal.length > 1 || ((tipo == 'diagonal') && (casa == 0 || casa == 56 || casa == 63 || casa == 07)))
//            break;

    }

    function chama(armazena_posicao_linha_coluna_diagonal, casa) {
        //se encontrou as duas digonais ou clicou em uma casa que esta na linha_coluna maior
        if ((armazena_posicao_linha_coluna_diagonal.length > 1) || ((tipo == 'diagonal') && (casa == 0 || casa == 56 || casa == 63 || casa == 07)) || (armazena_posicao_linha_coluna_diagonal.length > 0 && (tipo == 'linha' || tipo == 'coluna')))
        {
            //chama a funcao que faz a selecao das diagonais
            faz(armazena_posicao_linha_coluna_diagonal);
        }
    }

    function faz(armazena_posicao_linha_coluna) {

        for (var i = 0; i < armazena_posicao_linha_coluna.length; i++) {

            //posicao do vetor da linha_coluna  encontrado antes de chamar a funcao
            var l_linha_coluna_diagonal = armazena_posicao_linha_coluna[i].posicao_linha_coluna_diagonal;
            var posicao_casa = armazena_posicao_linha_coluna[i].posicao_casa;

            //marca de uma poiscao alem da casa até o final da linha_coluna
            for (var x = posicao_casa + 1; x < array_linha_coluna_diagonal[l_linha_coluna_diagonal].length; x++) {

                //verifica se a casa não esta vazia
                if (jQuery("#" + id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).html() != "")
                {
                    var p_cor = document.getElementById(id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).getElementsByTagName("img")[0].getAttribute("cor");

                    //se for uma peca minha, para
                    if (p_cor == cor)
                        break;
                    else {
                        
                        if (reiEmCheque(id_tabuleiro, "", array_linha_coluna_diagonal[l_linha_coluna_diagonal][x], "").cheque == -1 )
                            //se for do adversario, marca e depois para
                            jQuery("#" + id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).addClass("marca_movimento");
                    
                        //break;
                    }

                    break;

                } else {
                    if (reiEmCheque(id_tabuleiro, "", array_linha_coluna_diagonal[l_linha_coluna_diagonal][x], "").cheque == -1 )
                        //se for do adversario, marca e depois para
                        jQuery("#" + id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).addClass("marca_movimento");
                    break;
                }


            }

            //marca de de uma posicao antes da casa até o inicio da linha_coluna
            for (var x = posicao_casa - 1; x >= 0; x--) {

                //verifica se a casa não esta vazia
                if (jQuery("#" + id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).html() != "")
                {

                    var p_cor = document.getElementById(id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).getElementsByTagName("img")[0].getAttribute("cor");

                    //se for uma peca minha, para
                    if (p_cor == cor)
                        break;
                    else {
                        if (reiEmCheque(id_tabuleiro, "", array_linha_coluna_diagonal[l_linha_coluna_diagonal][x], "").cheque == -1 )
                            //se for do adversario, marca e depois para
                            jQuery("#" + id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).addClass("marca_movimento");
                        //break;
                    }
                    break;
                }
                else {

                    if (reiEmCheque(id_tabuleiro, "", array_linha_coluna_diagonal[l_linha_coluna_diagonal][x], "").cheque == -1 )
                        //se for do adversario, marca e depois para
                        jQuery("#" + id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).addClass("marca_movimento");
                    
                    break;

                }

            }
//
        }


        //MARCA PARA O MOVIMENTO DO ROQUE
        
        if (reiEmCheque(id_tabuleiro, "", parseInt(casa), "").cheque == -1 ){
            

            if (jQuery("#" + id_tabuleiro + "-" + casa + " img").attr("permite_roque") == "sim")
            {
                if (jQuery("#" + id_tabuleiro + "-" + (parseInt(casa) - 4) + " img").attr("permite_roque") == "sim")
                {
                     if (reiEmCheque(id_tabuleiro, "", (parseInt(casa) - 3), "").cheque == -1 && reiEmCheque(id_tabuleiro, "", (parseInt(casa) - 2), "").cheque == -1 )
                        if (jQuery("#" + id_tabuleiro + "-" + (parseInt(casa) - 3)).html() == "" && jQuery("#" + id_tabuleiro + "-" + (parseInt(casa) - 2)).html() == "")
                               jQuery("#" + id_tabuleiro + "-" + (parseInt(casa) - 2)).addClass("marca_movimento");

                }

                
                if (jQuery("#" + id_tabuleiro + "-" + (parseInt(casa) + 3) + " img").attr("permite_roque") == "sim")
                {
                    if ( reiEmCheque(id_tabuleiro, "", (parseInt(casa) +2), "").cheque == -1 && reiEmCheque(id_tabuleiro, "", (parseInt(casa) + 1), "").cheque == -1 )
                        if (jQuery("#" + id_tabuleiro + "-" + (parseInt(casa) + 2)).html() == "" && jQuery("#" + id_tabuleiro + "-" + (parseInt(casa) + 1)).html() == "")
                            jQuery("#" + id_tabuleiro + "-" + (parseInt(casa) + 2)).addClass("marca_movimento");

                }


            }
        
        }



    }

}
function marcaMovimentoL(casa, id_tabuleiro) {

    //cor das pecas do jogador
    var cor = jQuery("#" + id_tabuleiro + " [name='cor']").val();

    var posicao_linha = null;
    var posicao_coluna = null;


    for (var i = 0; i < linha.length; i++) {

        //retorna a posicao da casa na linha_coluna, ou -1 se nao encontrar nada
        var x = esta(casa, linha[i]);
        var y = esta(casa, coluna[i]);

        if (x > -1) {
            posicao_linha = x;
            chama()
        }


        if (y > -1) {
            posicao_coluna = y;
            chama()
        }


    }

    function chama() {
        if (posicao_coluna != null && posicao_linha != null) {

            //ESTA INVERTIDO, INVESTIGAR PORQUE
            faz(posicao_coluna, posicao_linha);

        }
    }


    function faz(p_linha, p_coluna) {

        var p = [];


        if (linha[p_linha - 2] != undefined) {

            if (linha[p_linha - 2][p_coluna + 1] != undefined)
                p[0] = jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 2][p_coluna + 1]);
            if (linha[p_linha - 2][p_coluna - 1] != undefined)
                p[1] = jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 2][p_coluna - 1]);

        }

        if (linha[p_linha - 1] != undefined) {

            if (linha[p_linha - 1][p_coluna + 2] != undefined)
                p[2] = jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna + 2]);
            if (linha[p_linha - 1][p_coluna - 2] != undefined)
                p[3] = jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna - 2]);

        }

        if (linha[p_linha + 1] != undefined) {

            if (linha[p_linha + 1][p_coluna + 2] != undefined)
                p[4] = jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna + 2]);
            if (linha[p_linha + 1][p_coluna - 2] != undefined)
                p[5] = jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna - 2]);

        }

        if (linha[p_linha + 2] != undefined) {

            if (linha[p_linha + 2][p_coluna + 1] != undefined)
                p[6] = jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 2][p_coluna + 1]);
            if (linha[p_linha + 2][p_coluna - 1] != undefined)
                p[7] = jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 2][p_coluna - 1]);

        }


        for (var i = 0; i < p.length; i++) {

            if (p[i] != undefined && p[i] != null) {
                if (p[i].html() != "")
                {
                    var obj_img = p[i][0].getElementsByTagName("img")[0];
                    var p_cor = ""

                    if (obj_img != undefined && obj_img != null)
                        p_cor = obj_img.getAttribute("cor");


                    //se for uma peca minha, para
                    if (p_cor == cor) {
                        //break;
                    }
                    else {
                        if (reiEmCheque(id_tabuleiro, parseInt(p[i][0].id.split("-")[1]), "", parseInt(casa)).cheque == -1 )
                        //se for do adversario, marca e depois para
                        p[i].addClass("marca_movimento");
                    }
                }
                else {
                    if (reiEmCheque(id_tabuleiro, parseInt(p[i][0].id.split("-")[1]), "", parseInt(casa)).cheque == -1 )
                    //se nao apenas seleciona a casa
                    p[i].addClass("marca_movimento");
                }
            }



        }


    }

}
function marcaMovimentoPeao(casa, id_tabuleiro) {

    //cor das pecas do jogador
    var cor = jQuery("#" + id_tabuleiro + " [name='cor']").val();

    var posicao_linha = null;
    var posicao_coluna = null;


    for (var i = 0; i < linha.length; i++) {

        //retorna a posicao da casa na linha_coluna, ou -1 se nao encontrar nada
        var x = esta(casa, linha[i]);
        var y = esta(casa, coluna[i]);

        if (x > -1) {
            posicao_linha = x;
            chama()
        }


        if (y > -1) {
            posicao_coluna = y;
            chama()
        }


    }

    function chama() {
        if (posicao_coluna != null && posicao_linha != null) {

            //ESTA INVERTIDO, INVESTIGAR PORQUE
            faz(posicao_coluna, posicao_linha);

        }
    }


    function faz(p_linha, p_coluna) {

        //var peao = [];
        //
        //armazena o elemento meu_peao
        var m_peao = (jQuery("#" + id_tabuleiro + "-" + linha[p_linha][p_coluna]));

        //pega a cor do peao
        var m_cor = m_peao[0].getElementsByTagName("img")[0].getAttribute("cor");

        if (m_cor == "branco") {
            //se esta na 6 coluna pode mecher 2 pra frente
            if (p_linha == 6) {
                //primeiro verifica se a casa da frente esta livre
                if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna]).html() == "") {
                    
                    if (reiEmCheque(id_tabuleiro, linha[p_linha - 1][p_coluna], "", parseInt(casa)).cheque == -1 )
                        jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna]).addClass("marca_movimento");
                        //depois se a ultima casa possivel esta livre
                    if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 2][p_coluna]).html() == "")
                        if (reiEmCheque(id_tabuleiro, linha[p_linha - 2][p_coluna], "", parseInt(casa)).cheque == -1 )
                            jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 2][p_coluna]).addClass("marca_movimento");
                }

            } else {
                if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna]).html() == "")
                    if (reiEmCheque(id_tabuleiro, linha[p_linha - 1][p_coluna], "", parseInt(casa)).cheque == -1 )
                        jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna]).addClass("marca_movimento");

            }
            //parte que marca as casas de captura
            //verifica se a coluna existe, pois pode ser a ultima ou a primeira coluna
            if (linha[p_linha - 1][p_coluna - 1] != undefined) {
                //verifica se existe alguma peca na casa
                if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna - 1]).html() != "") {
                    //verifica se a cor da peca e diferente da minha
                    if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna - 1])[0].getElementsByTagName("img")[0].getAttribute("cor") != m_cor)
                        if (reiEmCheque(id_tabuleiro, linha[p_linha - 1][p_coluna - 1], "", parseInt(casa)).cheque == -1 )
                            jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna - 1]).addClass("marca_movimento");

                }

            }

            if (linha[p_linha - 1][p_coluna + 1] != undefined) {
                //verifica se existe alguma peca na casa
                if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna + 1]).html() != "") {
                    //verifica se a cor da peca e diferente da minha
                    if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna + 1])[0].getElementsByTagName("img")[0].getAttribute("cor") != m_cor)
                        if (reiEmCheque(id_tabuleiro, linha[p_linha - 1][p_coluna + 1], "", parseInt(casa)).cheque == -1 )
                            jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna + 1]).addClass("marca_movimento");

                }

            }

            //marca as capturas em enpassant

            if (p_linha == 3) {

                if (linha[p_linha][p_coluna + 1] != undefined) {
                    if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha][p_coluna + 1] + " img[peca='peao']").attr("sofre_enpassant") == "sim")
                        if (reiEmCheque(id_tabuleiro, linha[p_linha - 1][p_coluna + 1], "", parseInt(casa)).cheque == -1 )
                            jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna + 1]).addClass("marca_movimento");

                }

                //marca as capturas em enpassant
                if (linha[p_linha][p_coluna - 1] != undefined) {
                    if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha][p_coluna - 1] + " img[peca='peao']").attr("sofre_enpassant") == "sim")
                        if (reiEmCheque(id_tabuleiro, linha[p_linha - 1][p_coluna - 1], "", parseInt(casa)).cheque == -1 )
                          jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna - 1]).addClass("marca_movimento");

                }

            }

        } else {
            if (p_linha == 1) {
                //primeiro verifica se a casa da frente esta livre
                if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna]).html() == "") {
                    if (reiEmCheque(id_tabuleiro, linha[p_linha + 1][p_coluna], "", parseInt(casa)).cheque == -1 )
                        jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna]).addClass("marca_movimento");
                    //depois se a ultima casa possivel esta livre
                    if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 2][p_coluna]).html() == "")
                        if (reiEmCheque(id_tabuleiro, linha[p_linha + 2][p_coluna], "", parseInt(casa)).cheque == -1 )
                            jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 2][p_coluna]).addClass("marca_movimento");
                }

            } else {
                if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna]).html() == "")
                    if (reiEmCheque(id_tabuleiro, linha[p_linha + 1][p_coluna], "", parseInt(casa)).cheque == -1 )
                        jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna]).addClass("marca_movimento");

            }

            //parte que marca as casas de captura
            //verifica se a coluna existe, pois pode ser a ultima ou a primeira coluna
            if (linha[p_linha + 1][p_coluna - 1] != undefined) {
                //verifica se existe alguma peca na casa
                if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna - 1]).html() != "") {
                    //verifica se a cor da peca e diferente da minha
                    if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna - 1])[0].getElementsByTagName("img")[0].getAttribute("cor") != m_cor)
                        if (reiEmCheque(id_tabuleiro, linha[p_linha + 1][p_coluna -1], "", parseInt(casa)).cheque == -1 )    
                            jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna - 1]).addClass("marca_movimento");

                }
            }

            if (linha[p_linha + 1][p_coluna + 1] != undefined) {
                //verifica se existe alguma peca na casa
                if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna + 1]).html() != "") {
                    //verifica se a cor da peca e diferente da minha
                    if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna + 1])[0].getElementsByTagName("img")[0].getAttribute("cor") != m_cor)
                        if (reiEmCheque(id_tabuleiro, linha[p_linha + 1][p_coluna+1], "", parseInt(casa)).cheque == -1 )    
                            jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna + 1]).addClass("marca_movimento");

                }
            }


            //marca as capturas em enpassant

            if (p_linha == 4) {

                if (linha[p_linha][p_coluna + 1] != undefined) {
                    if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha][p_coluna + 1] + " img[peca='peao']").attr("sofre_enpassant") == "sim")
                        if (reiEmCheque(id_tabuleiro, linha[p_linha + 1][p_coluna], "", parseInt(casa)).cheque == -1 )
                            jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna]).addClass("marca_movimento");

                }

                //marca as capturas em enpassant
                if (linha[p_linha][p_coluna - 1] != undefined) {
                    if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha][p_coluna - 1] + " img[peca='peao']").attr("sofre_enpassant") == "sim")
                        if (reiEmCheque(id_tabuleiro, linha[p_linha + 1][p_coluna -1], "", parseInt(casa)).cheque == -1 )
                            jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna - 1]).addClass("marca_movimento");

                }

            }

        }

    }

}

function esta(valor, array) {

    for (var i = 0; i <= array.length - 1; i++) {
        if (valor == array[i])
            return i;

        if (i == array.length - 1)
            return -1;
    }

}


function posicaoLinhaColuna(valor, tipo) {


    if (tipo == 'coluna') {

        for (var i = 0; i < coluna.length; i++) {

            if (esta(valor, coluna[i]) > -1) {
                return i;
            } else {
                if (i == coluna.length - 1)
                    return -1;
            }

        }

    } else {
        for (var i = 0; i < linha.length; i++) {

            if (esta(valor, linha[i]) > -1) {
                return i;
            } else {
                if (i == linha.length - 1)
                    return -1;
            }

        }

    }

}





function promove(peca, casaInicial, casaDestino, idAdversario, id_tabuleiro, cor) {

    enviaMovimento(idAdversario, id_tabuleiro, casaInicial, casaDestino, cor, id_tabuleiro.split("tab_")[1], "promocao", peca);
    $("#fechar_menu_promocao").trigger("click");


}


function reiEmCheque(id_tabuleiro, id_casa_atacada, id_casa_rei, id_casa_livre) {

    var minha_cor = jQuery("#" + id_tabuleiro + " [name='cor']").val();
    var cor_adversario = "";

    if (minha_cor == "branco")
        cor_adversario = "preto";
    else
        cor_adversario = "branco";





    var meu_rei = jQuery("#" + id_tabuleiro + "  div img[peca='rei'][cor='" + minha_cor + "']").parent("div");

    var casa_rei = meu_rei.attr("id").split("-")[1];

    var linha_rei = posicaoLinhaColuna(casa_rei, 'linha');
    var coluna_rei = posicaoLinhaColuna(casa_rei, 'coluna');


//    console.log("Meu rei: " + meu_rei);
//    console.log("Linha: " + linha_rei);
//    console.log("Coluna: " + coluna_rei);

    var arrayCasasAtacadas = [];


    if (id_casa_atacada == undefined || id_casa_atacada == "")
        id_casa_atacada = "";
    else
        id_casa_atacada = parseInt(id_casa_atacada);

    if (id_casa_rei == undefined || id_casa_rei == "")
        id_casa_rei = "";
    else
        id_casa_rei = parseInt(id_casa_rei);

    $("#" + id_tabuleiro + "  div img[cor='" + cor_adversario + "']").each(function () {

        var peca = $(this);
        var id_peca = peca.parent("div").attr("id").split("-")[1];

        if (id_casa_atacada != parseInt(id_peca)) {

            switch (peca.attr("peca")) {
                case 'dama':
                    arrayCasasAtacadas = casasAtacadasColunaDiagonal(id_tabuleiro, id_peca, cor_adversario, 'coluna', coluna, arrayCasasAtacadas, id_casa_atacada, id_casa_rei, id_casa_livre);
                    arrayCasasAtacadas = casasAtacadasColunaDiagonal(id_tabuleiro, id_peca, cor_adversario, 'linha', linha, arrayCasasAtacadas, id_casa_atacada, id_casa_rei, id_casa_livre);
                    arrayCasasAtacadas = casasAtacadasColunaDiagonal(id_tabuleiro, id_peca, cor_adversario, 'diagonal', diagonal, arrayCasasAtacadas, id_casa_atacada, id_casa_rei, id_casa_livre);
                    break;

                case 'bispo':
                    arrayCasasAtacadas = casasAtacadasColunaDiagonal(id_tabuleiro, id_peca, cor_adversario, 'diagonal', diagonal, arrayCasasAtacadas, id_casa_atacada, id_casa_rei, id_casa_livre);
                    break;

                case 'torre':
                    arrayCasasAtacadas = casasAtacadasColunaDiagonal(id_tabuleiro, id_peca, cor_adversario, 'coluna', coluna, arrayCasasAtacadas, id_casa_atacada, id_casa_rei, id_casa_livre);
                    arrayCasasAtacadas = casasAtacadasColunaDiagonal(id_tabuleiro, id_peca, cor_adversario, 'linha', linha, arrayCasasAtacadas, id_casa_atacada, id_casa_rei, id_casa_livre);
                    break;

                case 'cavalo':
                    arrayCasasAtacadas = casasAtacadasL(id_tabuleiro, id_peca, cor_adversario, arrayCasasAtacadas, id_casa_atacada, id_casa_livre);
                    break;

                case 'peao':
                    arrayCasasAtacadas = casasAtacadasPeao(id_tabuleiro, id_peca, cor_adversario, arrayCasasAtacadas, id_casa_atacada, id_casa_rei, id_casa_livre);
                    break;
                case 'rei':
                    arrayCasasAtacadas = casasAtacadasColunaDiagonalUmaCasa(id_tabuleiro, id_peca, cor_adversario, 'coluna', coluna, arrayCasasAtacadas, id_casa_atacada, id_casa_rei, id_casa_livre);
                    arrayCasasAtacadas = casasAtacadasColunaDiagonalUmaCasa(id_tabuleiro, id_peca, cor_adversario, 'linha', linha, arrayCasasAtacadas, id_casa_atacada, id_casa_rei, id_casa_livre);
                    arrayCasasAtacadas = casasAtacadasColunaDiagonalUmaCasa(id_tabuleiro, id_peca, cor_adversario, 'diagonal', diagonal, arrayCasasAtacadas, id_casa_atacada, id_casa_rei, id_casa_livre);
                    break;

            }
        }

    });

    if (id_casa_rei != "")
        casa_rei = id_casa_rei;


    var retorno = {
        casasAtacadas: arrayCasasAtacadas,
        cheque: esta(parseInt(casa_rei), arrayCasasAtacadas)
    }

    return retorno;

}

function reiEmChequeMate(id_tabuleiro) {

    var minha_cor = jQuery("#" + id_tabuleiro + " [name='cor']").val();
    var cor_adversario = "";

    if (minha_cor == "branco")
        cor_adversario = "preto";
    else
        cor_adversario = "branco";


    var meu_rei = jQuery("#" + id_tabuleiro + "  div img[peca='rei'][cor='" + minha_cor + "']").parent("div");

    var casa_rei = meu_rei.attr("id").split("-")[1];

    casa_rei = parseInt(casa_rei);

    var rei_adversario = jQuery("#" + id_tabuleiro + "  div img[peca='rei'][cor='" + cor_adversario + "']").parent("div");

    var casa_rei_adversario = rei_adversario.attr("id").split("-")[1];

    casa_rei = parseInt(casa_rei);
    casa_rei_adversario = parseInt(casa_rei_adversario);

//    var linha_rei = posicaoLinhaColuna(casa_rei, 'linha');
//    var coluna_rei = posicaoLinhaColuna(casa_rei, 'coluna');
//
//
////    console.log("Meu rei: " + meu_rei);
////    console.log("Linha: " + linha_rei);
////    console.log("Coluna: " + coluna_rei);

    var arrayCasasAtacadas = [];
    var arrayArrayCasasAtacadas = [];
    var arrayCasasAtacadasRei = [];

    var i = 0;

    $("#" + id_tabuleiro + "  div img[cor='" + minha_cor + "']").each(function () {
        
        var peca = $(this);
        var id_peca = peca.parent("div").attr("id").split("-")[1];


        switch (peca.attr("peca")) {
            case 'dama':
                
                arrayCasasAtacadas = [];
                
                arrayCasasAtacadas = casasAtacadasColunaDiagonal(id_tabuleiro, id_peca, minha_cor, 'coluna', coluna, arrayCasasAtacadas, "", casa_rei_adversario, "");
                arrayCasasAtacadas = casasAtacadasColunaDiagonal(id_tabuleiro, id_peca, minha_cor, 'linha', linha, arrayCasasAtacadas, "", casa_rei_adversario, "");
                arrayCasasAtacadas = casasAtacadasColunaDiagonal(id_tabuleiro, id_peca, minha_cor, 'diagonal', diagonal, arrayCasasAtacadas, "", casa_rei_adversario, "");
                
                var casasAtacadasPeca = {
                    casa_peca: parseInt(id_peca),
                    casasAtacadas: arrayCasasAtacadas
                }
                
                arrayArrayCasasAtacadas.push(casasAtacadasPeca);
                
                break;

            case 'bispo':
                
                arrayCasasAtacadas = [];
                
                arrayCasasAtacadas = casasAtacadasColunaDiagonal(id_tabuleiro, id_peca, minha_cor, 'diagonal', diagonal, arrayCasasAtacadas, "", casa_rei_adversario, "");
                
                var casasAtacadasPeca = {
                    casa_peca: parseInt(id_peca),
                    casasAtacadas: arrayCasasAtacadas
                }
                
                arrayArrayCasasAtacadas.push(casasAtacadasPeca);
          
                break;

            case 'torre':
                
                arrayCasasAtacadas = [];
                
                arrayCasasAtacadas = casasAtacadasColunaDiagonal(id_tabuleiro, id_peca, minha_cor, 'coluna', coluna, arrayCasasAtacadas, "", casa_rei_adversario, "");
                arrayCasasAtacadas = casasAtacadasColunaDiagonal(id_tabuleiro, id_peca, minha_cor, 'linha', linha, arrayCasasAtacadas, "", casa_rei_adversario, "");
                
                var casasAtacadasPeca = {
                    casa_peca: parseInt(id_peca),
                    casasAtacadas: arrayCasasAtacadas
                }
                
                arrayArrayCasasAtacadas.push(casasAtacadasPeca);
                
                break;

            case 'cavalo':
                
                arrayCasasAtacadas = [];
                
                arrayCasasAtacadas = casasAtacadasL(id_tabuleiro, id_peca, minha_cor, arrayCasasAtacadas, "", casa_rei_adversario, "");
                
                var casasAtacadasPeca = {
                    casa_peca: parseInt(id_peca),
                    casasAtacadas: arrayCasasAtacadas
                }
                
                arrayArrayCasasAtacadas.push(casasAtacadasPeca);
                
                break;

            case 'peao':
                
                arrayCasasAtacadas = [];
                
                arrayCasasAtacadas = casasAtacadasPeao(id_tabuleiro, id_peca, minha_cor, arrayCasasAtacadas, "", casa_rei_adversario, "");
                
                var casasAtacadasPeca = {
                    casa_peca: parseInt(id_peca),
                    casasAtacadas: arrayCasasAtacadas
                }
                
                arrayArrayCasasAtacadas.push(casasAtacadasPeca);
                
                break;
                
            case 'rei':
                
                //arrayCasasAtacadas = [];
                
                arrayCasasAtacadasRei = casasAtacadasColunaDiagonalUmaCasa(id_tabuleiro, id_peca, minha_cor, 'coluna', coluna, arrayCasasAtacadasRei, "", casa_rei_adversario, "");
                arrayCasasAtacadasRei = casasAtacadasColunaDiagonalUmaCasa(id_tabuleiro, id_peca, minha_cor, 'linha', linha, arrayCasasAtacadasRei, "", casa_rei_adversario, "");
                arrayCasasAtacadasRei = casasAtacadasColunaDiagonalUmaCasa(id_tabuleiro, id_peca, minha_cor, 'diagonal', diagonal, arrayCasasAtacadasRei, "", casa_rei_adversario, "");
                
//                var casasAtacadasPeca = {
//                    casa_peca: parseInt(id_peca),
//                    casasAtacadas: arrayCasasAtacadas
//                }
//                
//                arrayArrayCasasAtacadas.push(casasAtacadasPeca);
                
                break;

        }

    });

    var cheque_mate = true;

    var casasDeFugaCheque = [];




    for (var i = 0; i < arrayArrayCasasAtacadas.length; i++) {
        
        for ( var x = 0; x < arrayArrayCasasAtacadas[i].casasAtacadas.length; x++ ){
            
            var objCheque = reiEmCheque(id_tabuleiro, arrayArrayCasasAtacadas[i].casasAtacadas[x], "", arrayArrayCasasAtacadas[i].casa_peca);

            if (objCheque.cheque == -1) {
                casasDeFugaCheque.push(arrayArrayCasasAtacadas[i].casasAtacadas[x]);
                cheque_mate = false;


                console.log("Casa de fuga: " + arrayArrayCasasAtacadas[i].casasAtacadas[x]);
                //console.log("Minha cor: "+minha_cor);
            }
        
        }


    }

    for (var i = 0; i < arrayCasasAtacadasRei.length; i++) {
        var objCheque = reiEmCheque(id_tabuleiro, arrayCasasAtacadasRei[i], arrayCasasAtacadasRei[i], "");

        if (objCheque.cheque == -1) {
            casasDeFugaCheque.push(arrayCasasAtacadasRei[i]);
            cheque_mate = false;


            console.log("Casa de fuga rei: " + arrayCasasAtacadasRei[i]);
            //console.log("Minha cor: "+minha_cor);
        }
    }

    var obj_cheque_mate = {
        cheque_mate: cheque_mate,
        casasDeFuga: casasDeFugaCheque
    }

    return obj_cheque_mate;

}


function casasAtacadas(id_tabuleiro, casa, cor, tipo, array_linha_coluna_diagonal, arrayCasasAtacadas) {

}




function casasAtacadasColunaDiagonal(id_tabuleiro, casa, cor, tipo, array_linha_coluna_diagonal, arrayCasasAtacadas, id_casa_atacada, casa_do_rei, id_casa_livre) {

    var armazena_posicao_linha_coluna_diagonal = [];

    if (  casa_do_rei != "" && id_casa_atacada != "" ){
        
        console.log("Casa do rei: "+casa_do_rei);
        console.log("Casa atacada: "+id_casa_atacada);
        
    }


    for (var i = 0; i < array_linha_coluna_diagonal.length; i++) {

        //retorna a posicao da casa na linha_coluna, ou -1 se nao encontrar nada
        var x = esta(casa, array_linha_coluna_diagonal[i]);

        if (x > -1) {

            var obj_linha_coluna_diagonal = {
                posicao_casa: x,
                posicao_linha_coluna_diagonal: i
            }
            armazena_posicao_linha_coluna_diagonal.push(obj_linha_coluna_diagonal);
            chama(armazena_posicao_linha_coluna_diagonal, casa);

        }

//        if (armazena_posicao_linha_coluna_diagonal.length > 1 || ((tipo == 'diagonal') && (casa == 0 || casa == 56 || casa == 63 || casa == 07)))
//            break;

    }

    function chama(armazena_posicao_linha_coluna_diagonal, casa) {
        //se encontrou as duas digonais ou clicou em uma casa que esta na linha_coluna maior
        if ((armazena_posicao_linha_coluna_diagonal.length > 1) || ((tipo == 'diagonal') && (casa == 0 || casa == 56 || casa == 63 || casa == 07)) || (armazena_posicao_linha_coluna_diagonal.length > 0 && (tipo == 'linha' || tipo == 'coluna')))
        {
            //chama a funcao que faz a selecao das diagonais
            faz(armazena_posicao_linha_coluna_diagonal);
        }
    }

    function faz(armazena_posicao_linha_coluna) {

        for (var i = 0; i < armazena_posicao_linha_coluna.length; i++) {

            //posicao do vetor da linha_coluna  encontrado antes de chamar a funcao
            var l_linha_coluna_diagonal = armazena_posicao_linha_coluna[i].posicao_linha_coluna_diagonal;
            var posicao_casa = armazena_posicao_linha_coluna[i].posicao_casa;

            //marca de uma poiscao alem da casa até o final da linha_coluna
            for (var x = posicao_casa + 1; x < array_linha_coluna_diagonal[l_linha_coluna_diagonal].length; x++) {

                //verifica se a casa não esta vazia
                if ((jQuery("#" + id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).html() != "" || id_casa_atacada == array_linha_coluna_diagonal[l_linha_coluna_diagonal][x] ) &&  id_casa_livre != array_linha_coluna_diagonal[l_linha_coluna_diagonal][x] )
                {
                    var p_cor = ""; 
                    
                    if ( id_casa_atacada != array_linha_coluna_diagonal[l_linha_coluna_diagonal][x] )
                        document.getElementById(id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).getElementsByTagName("img")[0].getAttribute("cor");

                    //se for uma peca minha, para
                    if (p_cor == cor)
                        break;
                    else {
                        //se for do adversario, marca e depois para
                        
                        
                        arrayCasasAtacadas.push(array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]);
                        
                        if ( id_casa_atacada != casa_do_rei || id_casa_atacada == "" )
                            break;
                    }


                } else {
                    arrayCasasAtacadas.push(array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]);

                }


            }

            //marca de de uma posicao antes da casa até o inicio da linha_coluna
            for (var x = posicao_casa - 1; x >= 0; x--) {

                //verifica se a casa não esta vazia
                if ( (jQuery("#" + id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).html() != "" || id_casa_atacada == array_linha_coluna_diagonal[l_linha_coluna_diagonal][x] ) &&  id_casa_livre != array_linha_coluna_diagonal[l_linha_coluna_diagonal][x])
                {

                    var p_cor = ""; 
                    
                    if ( id_casa_atacada != array_linha_coluna_diagonal[l_linha_coluna_diagonal][x] )
                        document.getElementById(id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]).getElementsByTagName("img")[0].getAttribute("cor");

                    //se for uma peca minha, para
                    if (p_cor == cor)
                        break;
                    else {
                        //se for do adversario, marca e depois para
                        arrayCasasAtacadas.push(array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]);
                        
                        if ( id_casa_atacada != casa_do_rei || id_casa_atacada == "" )
                            break
                    }

                }
                else {

                    //se nao apenas seleciona a casa
                    arrayCasasAtacadas.push(array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]);

                }

            }
//
        }
    }

    return arrayCasasAtacadas;

}

function casasAtacadasColunaDiagonalUmaCasa(id_tabuleiro, casa, cor, tipo, array_linha_coluna_diagonal, arrayCasasAtacadas, id_casa_atacada, casa_do_rei) {

    //ira armazena as posicoes das duas diagonais quem cruzam a casa
    var armazena_posicao_linha_coluna_diagonal = [];


    for (var i = 0; i < array_linha_coluna_diagonal.length; i++) {

        //retorna a posicao da casa na linha_coluna, ou -1 se nao encontrar nada
        var x = esta(casa, array_linha_coluna_diagonal[i]);

        if (x > -1) {

            var obj_linha_coluna_diagonal = {
                posicao_casa: x,
                posicao_linha_coluna_diagonal: i
            }
            armazena_posicao_linha_coluna_diagonal.push(obj_linha_coluna_diagonal);
            chama(armazena_posicao_linha_coluna_diagonal, casa);

        }

    }

    function chama(armazena_posicao_linha_coluna_diagonal, casa) {
        //se encontrou as duas digonais ou clicou em uma casa que esta na linha_coluna maior
        if ((armazena_posicao_linha_coluna_diagonal.length > 1) || ((tipo == 'diagonal') && (casa == 0 || casa == 56 || casa == 63 || casa == 07)) || (armazena_posicao_linha_coluna_diagonal.length > 0 && (tipo == 'linha' || tipo == 'coluna')))
        {
            //chama a funcao que faz a selecao das diagonais
            faz(armazena_posicao_linha_coluna_diagonal);
        }
    }

    function faz(armazena_posicao_linha_coluna) {

        for (var i = 0; i < armazena_posicao_linha_coluna.length; i++) {

            //posicao do vetor da linha_coluna  encontrado antes de chamar a funcao
            var l_linha_coluna_diagonal = armazena_posicao_linha_coluna[i].posicao_linha_coluna_diagonal;
            var posicao_casa = armazena_posicao_linha_coluna[i].posicao_casa;

            //marca de uma poiscao alem da casa até o final da linha_coluna
            for (var x = posicao_casa + 1; x < array_linha_coluna_diagonal[l_linha_coluna_diagonal].length; x++) {

                var casa = jQuery("#" + id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]);

                        if ( casa.html() != "" ){
                            if ( casa[0].getElementsByTagName("img")[0].getAttribute("cor") == cor ){
                                if ( parseInt(casa[0].id.split("-")[1]) != parseInt(id_casa_atacada) )
                                    break;
                            }
                        } 

                                arrayCasasAtacadas.push(array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]);
                                break;
                        

            }

            for (var x = posicao_casa - 1; x >= 0; x--) {

                       var casa = jQuery("#" + id_tabuleiro + "-" + array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]);

                        if ( casa.html() != "" ){
                            if ( casa[0].getElementsByTagName("img")[0].getAttribute("cor") == cor ){
                                if ( parseInt(casa[0].id.split("-")[1]) != parseInt(id_casa_atacada) )
                                    break;
                            }
                        } 

                        arrayCasasAtacadas.push(array_linha_coluna_diagonal[l_linha_coluna_diagonal][x]);
                        break;
            }
//
        }
        

    }

    return arrayCasasAtacadas;

}



function casasAtacadasL(id_tabuleiro, casa, cor, arrayCasasAtacadas, id_casa_atacada, casa_do_rei) {


    var posicao_linha = null;
    var posicao_coluna = null;


    for (var i = 0; i < linha.length; i++) {

        //retorna a posicao da casa na linha_coluna, ou -1 se nao encontrar nada
        var x = esta(casa, linha[i]);
        var y = esta(casa, coluna[i]);

        if (x > -1) {
            posicao_linha = x;
            chama();
        }
        if (y > -1) {
            posicao_coluna = y;
            chama();
        }

    }

    function chama() {
        if (posicao_coluna != null && posicao_linha != null) {

            //ESTA INVERTIDO, INVESTIGAR PORQUE
            faz(posicao_coluna, posicao_linha);

        }
    }

    function faz(p_linha, p_coluna) {

        var p = [];

        if (linha[p_linha - 2] != undefined) {

            if (linha[p_linha - 2][p_coluna + 1] != undefined)
                p[0] = jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 2][p_coluna + 1]);
            if (linha[p_linha - 2][p_coluna - 1] != undefined)
                p[1] = jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 2][p_coluna - 1]);

        }

        if (linha[p_linha - 1] != undefined) {

            if (linha[p_linha - 1][p_coluna + 2] != undefined)
                p[2] = jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna + 2]);
            if (linha[p_linha - 1][p_coluna - 2] != undefined)
                p[3] = jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna - 2]);

        }

        if (linha[p_linha + 1] != undefined) {

            if (linha[p_linha + 1][p_coluna + 2] != undefined)
                p[4] = jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna + 2]);
            if (linha[p_linha + 1][p_coluna - 2] != undefined)
                p[5] = jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna - 2]);

        }

        if (linha[p_linha + 2] != undefined) {

            if (linha[p_linha + 2][p_coluna + 1] != undefined)
                p[6] = jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 2][p_coluna + 1]);
            if (linha[p_linha + 2][p_coluna - 1] != undefined)
                p[7] = jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 2][p_coluna - 1]);

        }

        for (var i = 0; i < p.length; i++) {

            if (p[i] != undefined && p[i] != null) {
                if (p[i].html() != "")
                {
                    var obj_img = p[i][0].getElementsByTagName("img")[0];
                    var p_cor = ""

                    if (obj_img != undefined && obj_img != null)
                        p_cor = obj_img.getAttribute("cor");

                    if (id_casa_atacada == "") {
                        //se for uma peca minha, para
                        if (p_cor == cor) {
                            //break;
                        }
                        else {
                            //se for do adversario, marca e depois para
                            arrayCasasAtacadas.push(parseInt(p[i].attr("id").split("-")[1]));
                        }
                    } else {
                        //if ( id_casa_atacada == parseInt(p[i].attr("id").split("-")[1]) ){
                        //    if (casa_do_rei == id_casa_atacada)
                        //        arrayCasasAtacadas.push(parseInt(p[i].attr("id").split("-")[1]));

                        //}else{
                        arrayCasasAtacadas.push(parseInt(p[i].attr("id").split("-")[1]));
                        //}
                    }



                }
                else {
                    if (id_casa_atacada == "")
                        //se nao apenas seleciona a casa
                        arrayCasasAtacadas.push(parseInt(p[i].attr("id").split("-")[1]));
                    else
                        //if ( id_casa_atacada != parseInt(p[i].attr("id").split("-")[1]) )
                        arrayCasasAtacadas.push(parseInt(p[i].attr("id").split("-")[1]));
                    //else
                    //    if (casa_do_rei == id_casa_atacada)
                    //        arrayCasasAtacadas.push(parseInt(p[i].attr("id").split("-")[1]));

                }
            }

        }

    }

    return arrayCasasAtacadas;

}


function casasAtacadasPeao(id_tabuleiro, casa, cor, arrayCasasAtacadas, id_casa_atacada, id_casa_rei_adversario) {

    var posicao_linha = null;
    var posicao_coluna = null;


    for (var i = 0; i < linha.length; i++) {

        //retorna a posicao da casa na linha_coluna, ou -1 se nao encontrar nada
        var x = esta(casa, linha[i]);
        var y = esta(casa, coluna[i]);

        if (x > -1) {
            posicao_linha = x;
            chama()
        }


        if (y > -1) {
            posicao_coluna = y;
            chama()
        }


    }

    function chama() {
        if (posicao_coluna != null && posicao_linha != null) {

            //ESTA INVERTIDO, INVESTIGAR PORQUE
            faz(posicao_coluna, posicao_linha);

        }
    }


    function faz(p_linha, p_coluna) {

        //pega a cor do peao
        var m_cor = cor;

        if (m_cor == "branco") {

            if (p_linha == 6) {
                //primeiro verifica se a casa da frente esta livre
                if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna]).html() == "") {

                    if (linha[p_linha - 1][p_coluna] != id_casa_atacada) {
                        arrayCasasAtacadas.push(linha[p_linha - 1][p_coluna]);
                        //depois se a ultima casa possivel esta livre

                        if (linha[p_linha - 2][p_coluna] != id_casa_atacada) {
                            if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 2][p_coluna]).html() == "")
                                arrayCasasAtacadas.push(linha[p_linha - 2][p_coluna]);
                        }
                    }

                }

            } else {

                if (linha[p_linha - 1][p_coluna] != id_casa_atacada) {
                    if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna]).html() == "")
                        arrayCasasAtacadas.push(linha[p_linha - 1][p_coluna]);
                }



            }


            //parte que marca as casas de captura
            //verifica se a coluna existe, pois pode ser a ultima ou a primeira coluna
            if (linha[p_linha - 1][p_coluna - 1] != undefined) {
                //verifica se a cor da peca e diferente da minha
                if ((jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna - 1] + " img[name='cor']").attr("cor") != m_cor && jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna - 1] + " img").attr("cor") != undefined) || id_casa_rei_adversario == linha[p_linha - 1][p_coluna - 1])
                    if (id_casa_atacada == "")
                        arrayCasasAtacadas.push(linha[p_linha - 1][p_coluna - 1]);
                    else
                    if (id_casa_atacada == linha[p_linha - 1][p_coluna - 1]) {
                        if (id_casa_rei_adversario == linha[p_linha - 1][p_coluna - 1])
                            arrayCasasAtacadas.push(linha[p_linha - 1][p_coluna - 1]);
                    } else
                        arrayCasasAtacadas.push(linha[p_linha - 1][p_coluna - 1]);


            }
            if (linha[p_linha - 1][p_coluna + 1] != undefined) {
                if ((jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna + 1] + " img").attr("cor") != m_cor && jQuery("#" + id_tabuleiro + "-" + linha[p_linha - 1][p_coluna + 1] + " img").attr("cor") != undefined) || id_casa_rei_adversario == linha[p_linha - 1][p_coluna + 1])
                    if (id_casa_atacada == "")
                        arrayCasasAtacadas.push(linha[p_linha - 1][p_coluna + 1]);
                    else
                    if (id_casa_atacada == linha[p_linha - 1][p_coluna + 1])
                    {
                        if (id_casa_rei_adversario == linha[p_linha - 1][p_coluna + 1])
                            arrayCasasAtacadas.push(linha[p_linha - 1][p_coluna + 1]);

                    } else
                        arrayCasasAtacadas.push(linha[p_linha - 1][p_coluna + 1]);

            }


        } else {


            if (linha[p_linha + 1][p_coluna] != id_casa_atacada) {


                if (p_linha == 1) {
                    //primeiro verifica se a casa da frente esta livre
                    if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna]).html() == "") {
                        arrayCasasAtacadas.push(linha[p_linha + 1][p_coluna]);
                        //depois se a ultima casa possivel esta livre

                        if (linha[p_linha + 2][p_coluna] != id_casa_atacada)
                            if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 2][p_coluna]).html() == "")
                                arrayCasasAtacadas.push(linha[p_linha + 2][p_coluna]);
                    }

                } else {
                    if (jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna]).html() == "")
                        arrayCasasAtacadas.push(linha[p_linha + 1][p_coluna]);

                }

            }


            //parte que marca as casas de captura
            //verifica se a coluna existe, pois pode ser a ultima ou a primeira coluna
            if (linha[p_linha + 1][p_coluna - 1] != undefined) {

                //verifica se a cor da peca e diferente da minha
                if ((jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna - 1] + " img").attr("cor") != m_cor && jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna - 1] + " img").attr("cor") != undefined) || id_casa_rei_adversario == linha[p_linha + 1][p_coluna - 1])
                    if (id_casa_atacada == "")
                        arrayCasasAtacadas.push(linha[p_linha + 1][p_coluna - 1]);
                    else
                    if (id_casa_atacada == linha[p_linha + 1][p_coluna - 1]) {
                        if (id_casa_rei_adversario == linha[p_linha + 1][p_coluna - 1])
                            arrayCasasAtacadas.push(linha[p_linha + 1][p_coluna - 1]);
                    } else
                        arrayCasasAtacadas.push(linha[p_linha + 1][p_coluna - 1]);


            }

            if (linha[p_linha + 1][p_coluna + 1] != undefined) {


                var valor = linha[p_linha + 1][p_coluna + 1];
                //verifica se a cor da peca e diferente da minha
                if ((jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna + 1] + " img").attr("cor") != m_cor && jQuery("#" + id_tabuleiro + "-" + linha[p_linha + 1][p_coluna + 1] + " img").attr("cor") != undefined) || id_casa_rei_adversario == linha[p_linha + 1][p_coluna + 1])
                    if (id_casa_atacada == "")
                        arrayCasasAtacadas.push(linha[p_linha + 1][p_coluna + 1]);
                    else
                    if (id_casa_atacada == linha[p_linha + 1][p_coluna + 1]) {
                        if (id_casa_rei_adversario == linha[p_linha + 1][p_coluna + 1])
                            arrayCasasAtacadas.push(linha[p_linha + 1][p_coluna + 1]);

                    } else
                        arrayCasasAtacadas.push(linha[p_linha + 1][p_coluna + 1]);

            }

        }

    }

    return arrayCasasAtacadas;

}

//$(document).ready(
//	function () {
//		$('.casa.marca_movimento').sortable(
//			{
//				accept			: 'peca',
//				helperclass		: 'dragAjuda',
//				//activeclass 	: 'dragAtivo',
//				//hoverclass 		: 'dragHover',
//				opacity			: 0.7,
//				//handle			: 'h2',
//				onChange 		: function()
//				{	    			 
//					//serialEsq = $.SortSerialize('drop-esquerda');
//					//serialDir = $.SortSerialize('drop-direita');
//				},
//				onStart : function()
//				{
//					$.iAutoscroller.start(this, $('body'));
//				},
//				onStop : function()
//				{
//					$.iAutoscroller.stop();
//				}
//			}
//		);
//	}

  


//);

//$(function() {
//    $( ".casa.marca_movimento" ).sortable();
//    $( ".peca" ).disableSelection();
//  });
  
  
//    $(function() {
//    $( ".casa.marca_movimento" ).draggable();
//  });


 function mensagem(){
     
     
     
    var mensagem = '<div class="direct-chat-msg">'+
                        '<div class="direct-chat-info clearfix">'+
                            '<span class="direct-chat-name pull-left">Alexander Pierce</span>'+
                            '<span class="direct-chat-timestamp pull-right">23 Jan 5:37 pm</span>'+
                        '</div>'+
                        '<img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image" />'+
                        '<div class="direct-chat-text">'+$('#msg-1').val()+'</div>'+
                    '</div>';
     
     $('#cht1').append(mensagem);
     $('#msg-1').val("");
     document.getElementById('cht1').scrollTop = 9999999;
     
 }