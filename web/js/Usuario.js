/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//atualiza_lista_status();

function menuUsuario(id_usuario) {

    if (jQuery("#conf_" + id_usuario).val() != "0") {
        para_pisca(id_usuario);
        mostraTabuleiro(jQuery("#conf_" + id_usuario).val());
    } else {
        $("#menu_adversario").reveal();
        $("#id_adversario").val(id_usuario);
        $("#nome_adversario").text(jQuery("#jogador_" + id_usuario).text().replace("ON", "").replace("OFF", ""));
    }


}

function menuConvite(id_usuario) {
    $("#convite_partida").reveal();
    $("#id_adversario_convidou").val(id_usuario);
    $("#nome_adversario_convidou").text(jQuery("#jogador_" + id_usuario).text().replace("ON", "").replace("OFF", ""));
}

function proporConfronto(id_adversario) {

    var request = $.ajax({
        url: "usuario",
        method: "post",
        data: {acao: "convite_confronto", id_adversario: id_adversario},
        dataType: "text",
        success: function (msg) {

            if (msg == "ok")
                $("#fechar_menu_adversario").trigger("click");
        }
    });

}

function aceitarConfronto(id_adversario) {

    var request = $.ajax({
        url: "usuario",
        method: "post",
        data: {acao: "aceitar_convite_confronto", id_adversario: id_adversario},
        dataType: "text",
        success: function (msg) {

            if (msg == "ok")
                $("#fechar_menu_confronto").trigger("click");
        }
    });

}


function logof() {

    var j = jQuery.noConflict();

    var request = j.ajax({
        url: "usuario",
        method: "post",
        data: {acao: "logof"},
        dataType: "text",
        success: function (msg) {
            window.location.href = "login.jsp";
        }
    });


    request.fail(function (jqXHR, textStatus) {
        alert("Falha ao efetuar logof: " + textStatus);
    });

}

function login() {

    var login = $("#txt_login").val();
    var senha = $("#txt_senha").val();

    var request = $.ajax({
        url: "usuario",
        method: "post",
        data: {acao: "login", login: login, senha: senha},
        dataType: "text",
        success: function (msg) {

            if (msg == "ok")
                window.location.href = "index.jsp";
            else
                alert("Usuario não encontrado!");
        }
    });


    request.fail(function (jqXHR, textStatus) {
        alert("Falha ao efetuar login: " + textStatus);
    });

}

function atualiza_lista_status() {

    var request = $.ajax({
        url: "usuario",
        method: "post",
        data: {acao: "atualiza_lista_status", id_usuario: id_usuario},
        dataType: "text"

    });

}

function inicia_pisca(id_usuario) {

    var piscando = setInterval(function () {
        if ($("#jogador_" + id_usuario).hasClass("pisca")) {
            $("#jogador_" + id_usuario).removeClass("pisca");
        } else {
            $("#jogador_" + id_usuario).addClass("pisca");
        }
    }, 400);

    var usuarioPiscando = {
        id_usuario: id_usuario,
        pisca: piscando

    }

    pisca_usuario.push(usuarioPiscando);

}

function para_pisca(id_usuario) {

    if (pisca_usuario != null && pisca_usuario.length > 0) {
        for (var i = 0; i < pisca_usuario.length; i++) {
            if (pisca_usuario[i].id_usuario == id_usuario)
                clearTimeout(pisca_usuario[i].pisca);
            $("#jogador_" + id_usuario).removeClass("pisca");
            //$("#jogador_" + id_usuario).css("opacity", "1");
        }
    }
}

function estaOnLine(id_usuario) {
    $("#status_" + id_usuario).text("ON");
    $("#status_" + id_usuario).css('color', 'green');
}

function estaOffLine(id_usuario) {
    $("#status_" + id_usuario).text("OFF");
    $("#status_" + id_usuario).css('color', 'red');
}