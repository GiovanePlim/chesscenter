/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// wait for the DOM to be loaded 
$(document).ready(function () {
    // bind 'myForm' and provide a simple callback function 
    $("#form_cadastro").submit(function (event) {


        event.preventDefault();

        // Stop form from submitting normally
        
        // Get some values from elements on the page:
        var $form = $(this),
                nome = $form.find("input[name='txt_nome']").val(),
                senha = $form.find("input[name='txt_senha']").val(),
                senha_repetida = $form.find("input[name='txt_repete_senha']").val(),
                email = $form.find("input[name='txt_email']").val(),
                login = $form.find("input[name='txt_login']").val(),
                url = $form.attr("action");


        showLoad("load");

        if (!IsEmail(email)) {
            $('#aviso_email_invalido').show();
            $('#aviso_email_existe').hide();
            hideLoad();
        } else
        if (senha != senha_repetida) {
            $('#aviso_senhas_diferentes').show();
        } else {

            //Send the data using post
            //var posting = $.post(url, {nome: nome, login: login, senha: senha, email: email});

            var request = $.ajax({
                url: url,
                method: "post",
                data: {acao: "cadastrar", nome: nome, login: login, senha: senha, email: email},
                dataType: "text"
            });

            request.done(function (resposta) {
                
                hideLoad();
                
                if (resposta == "ok") {
                    location.href = "index.jsp";
                } else
                if (resposta == "email_existente") {
                    $('#aviso_email_invalido').hide();
                    $('#aviso_email_existe').show();
                }
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Falha ao cadastrar: " + textStatus);
            });

        }
    });

});


function valida() {
    if ($('#txt_repete_senha').val() != $('#txt_senha').val()) {
        $('#aviso_senhas_diferentes').show();
        $('#aviso_senhas_iguais').hide();
    }
    else {
        $('#aviso_senhas_diferentes').hide();
        $('#aviso_senhas_iguais').show();
    }

}